-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
-- -----------------------------------------------------
-- Schema cosmetics_cf
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema cosmetics_cf
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `cosmetics_cf` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ;
USE `cosmetics_cf` ;

-- -----------------------------------------------------
-- Table `cosmetics_cf`.`accreditors`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `cosmetics_cf`.`accreditors` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  `abbreviation` VARCHAR(45) NOT NULL,
  `start_url` VARCHAR(45) NOT NULL,
  `domain` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `name_UNIQUE` (`name` ASC) VISIBLE,
  UNIQUE INDEX `abbreviation_UNIQUE` (`abbreviation` ASC) VISIBLE,
  UNIQUE INDEX `start_url_UNIQUE` (`start_url` ASC) VISIBLE,
  UNIQUE INDEX `domain_UNIQUE` (`domain` ASC) VISIBLE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;


-- -----------------------------------------------------
-- Table `cosmetics_cf`.`brands`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `cosmetics_cf`.`brands` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `brand_name_UNIQUE` (`name` ASC) VISIBLE)
ENGINE = InnoDB
AUTO_INCREMENT = 46570
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;


-- -----------------------------------------------------
-- Table `cosmetics_cf`.`cosmetics_types`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `cosmetics_cf`.`cosmetics_types` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `name_UNIQUE` (`name` ASC) VISIBLE)
ENGINE = InnoDB
AUTO_INCREMENT = 455
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;


-- -----------------------------------------------------
-- Table `cosmetics_cf`.`brands_cosmetics_types`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `cosmetics_cf`.`brands_cosmetics_types` (
  `brand_id` INT NOT NULL,
  `cosmetics_type_id` INT NOT NULL,
  PRIMARY KEY (`brand_id`, `cosmetics_type_id`),
  INDEX `fk_brands_has_cosmetics_type_cosmetics_type1_idx` (`cosmetics_type_id` ASC) VISIBLE,
  INDEX `fk_brands_has_cosmetics_type_brands1_idx` (`brand_id` ASC) VISIBLE,
  CONSTRAINT `fk_brands_has_cosmetics_type_brands1`
    FOREIGN KEY (`brand_id`)
    REFERENCES `cosmetics_cf`.`brands` (`id`),
  CONSTRAINT `fk_brands_has_cosmetics_type_cosmetics_type1`
    FOREIGN KEY (`cosmetics_type_id`)
    REFERENCES `cosmetics_cf`.`cosmetics_types` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;


-- -----------------------------------------------------
-- Table `cosmetics_cf`.`certificates_attributes`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `cosmetics_cf`.`certificates_attributes` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `name_UNIQUE` (`name` ASC) VISIBLE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;


-- -----------------------------------------------------
-- Table `cosmetics_cf`.`cfi`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `cosmetics_cf`.`cfi` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `brand_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_cfi_brands1_idx` (`brand_id` ASC) VISIBLE,
  CONSTRAINT `fk_cfi_brands1`
    FOREIGN KEY (`brand_id`)
    REFERENCES `cosmetics_cf`.`brands` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;


-- -----------------------------------------------------
-- Table `cosmetics_cf`.`cfk`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `cosmetics_cf`.`cfk` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `brand_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_cfk_brands1_idx` (`brand_id` ASC) VISIBLE,
  UNIQUE INDEX `brand_id_UNIQUE` (`brand_id` ASC) VISIBLE,
  CONSTRAINT `fk_cfk_brands1`
    FOREIGN KEY (`brand_id`)
    REFERENCES `cosmetics_cf`.`brands` (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 232
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;


-- -----------------------------------------------------
-- Table `cosmetics_cf`.`cfk_certificates_attributes`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `cosmetics_cf`.`cfk_certificates_attributes` (
  `cfk_id` INT NOT NULL,
  `certificates_attributes_id` INT NOT NULL,
  PRIMARY KEY (`cfk_id`, `certificates_attributes_id`),
  INDEX `fk_cfk_has_certificates_attributes_certificates_attributes1_idx` (`certificates_attributes_id` ASC) VISIBLE,
  INDEX `fk_cfk_has_certificates_attributes_cfk1_idx` (`cfk_id` ASC) VISIBLE,
  CONSTRAINT `fk_cfk_has_certificates_attributes_certificates_attributes1`
    FOREIGN KEY (`certificates_attributes_id`)
    REFERENCES `cosmetics_cf`.`certificates_attributes` (`id`),
  CONSTRAINT `fk_cfk_has_certificates_attributes_cfk1`
    FOREIGN KEY (`cfk_id`)
    REFERENCES `cosmetics_cf`.`cfk` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;


-- -----------------------------------------------------
-- Table `cosmetics_cf`.`cfk_certified_not`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `cosmetics_cf`.`cfk_certified_not` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `brand_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_cfk_certified_not_brands1_idx` (`brand_id` ASC) VISIBLE,
  UNIQUE INDEX `brand_id_UNIQUE` (`brand_id` ASC) VISIBLE,
  CONSTRAINT `fk_cfk_certified_not_brands1`
    FOREIGN KEY (`brand_id`)
    REFERENCES `cosmetics_cf`.`brands` (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 270
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;


-- -----------------------------------------------------
-- Table `cosmetics_cf`.`lb`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `cosmetics_cf`.`lb` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `brand_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_lb_brands1_idx` (`brand_id` ASC) VISIBLE,
  CONSTRAINT `fk_lb_brands1`
    FOREIGN KEY (`brand_id`)
    REFERENCES `cosmetics_cf`.`brands` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;


-- -----------------------------------------------------
-- Table `cosmetics_cf`.`peta`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `cosmetics_cf`.`peta` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `brand_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_peta_brands1_idx` (`brand_id` ASC) VISIBLE,
  CONSTRAINT `fk_peta_brands1`
    FOREIGN KEY (`brand_id`)
    REFERENCES `cosmetics_cf`.`brands` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
