
def get_credentials(path):
    with open(path, mode='r') as txt_file:
        lines = []
        for line in txt_file:
            lines.append(line.strip('\n'))
    if not lines:
        raise ValueError('File is empty')
    return lines
