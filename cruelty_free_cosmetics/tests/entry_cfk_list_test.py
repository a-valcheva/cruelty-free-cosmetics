import unittest

# from cruelty_free_cosmetics.models.entry_cfk import EntryCFK
from models.entry_cfk_list import EntryCFKList, EntryCFK

class Constructor_Should(unittest.TestCase):
    def test_sets_attributes(self):
        # Arrange & Act
        e = EntryCFK('Brand Name', True, ['certificate1'], [])
        entry_cfk_list = EntryCFKList([e])

        # Assert
        self.assertEqual('Brand Name', entry_cfk_list.collection[0].name)
        self.assertEqual(True, entry_cfk_list.collection[0].is_cf)
        self.assertEqual(['certificate1'], entry_cfk_list.collection[0].certificates_attributes)
        self.assertEqual([], entry_cfk_list.collection[0].cosmetics_types)

    def test_sets_attributes_empty(self):
        # Arrange & Act
        entry_cfk_list = EntryCFKList()

        # Assert
        self.assertEqual([], entry_cfk_list.collection)

class Property_Should(unittest.TestCase):
    def test_length_property(self):
        # Arrange
        entry_cfk_list = EntryCFKList([])

        # Act
        length = entry_cfk_list.lendth

        # Assert
        self.assertEqual(0, length)

class Method_add_item_Should(unittest.TestCase):
    def test_add_item_succesfully(self):
        # Arrange
        entry_cfk_list = EntryCFKList()
        e = EntryCFK('Brand Name', True, ['certificate1'], [])

        # Act
        entry_cfk_list.add_item(e)
        
        # Assert
        self.assertEqual('Brand Name', entry_cfk_list.collection[0].name)
        self.assertEqual(True, entry_cfk_list.collection[0].is_cf)
        self.assertEqual(['certificate1'], entry_cfk_list.collection[0].certificates_attributes)
        self.assertEqual([], entry_cfk_list.collection[0].cosmetics_types)

    def test_add_item_fail_with_typeerror(self):
        with self.assertRaises(TypeError):
            entry_cfk_list = EntryCFKList()
            entry_cfk_list.add_item('string')

class Method_get_spider_brands_Should(unittest.TestCase):
    def test_get_spider_brands_succesfully(self):
        # Arrange
        entry_cfk_list = EntryCFKList([EntryCFK('Brand Name', True, ['certificate1'], [])])

        # Act
        collection = entry_cfk_list.get_spider_brands()
        
        # Assert
        self.assertEqual(('Brand Name',), collection)

class Method_get_spider_brands_as_tuples_Should(unittest.TestCase):
    def test_get_spider_brands_as_tuples_succesfully(self):
        # Arrange
        entry_cfk_list = EntryCFKList([EntryCFK('Brand Name', True, ['certificate1'], [])])

        # Act
        collection = entry_cfk_list.get_spider_brands_as_tuples()
        
        # Assert
        self.assertEqual((('Brand Name',),), collection)

class Method_get_cosmetics_types_as_tuples_Should(unittest.TestCase):
    def test_get_cosmetics_types_as_tuples_succesfully(self):
        # Arrange
        entry_cfk_list = EntryCFKList([EntryCFK('Brand Name', True, [], ['cosmeticType1'])])

        # Act
        collection = entry_cfk_list.get_cosmetics_types_as_tuples()
        
        # Assert
        self.assertEqual((('cosmeticType1',),), collection)

class Method_get_unique_cosmetics_types_Should(unittest.TestCase):
    def test_get_unique_cosmetics_types_succesfully(self):
        # Arrange
        entry_cfk_list = EntryCFKList([EntryCFK('Brand Name', True, [], ['cosmeticType1', 'cosmeticType1'])])

        # Act
        collection = entry_cfk_list.get_unique_cosmetics_types()
        
        # Assert
        self.assertEqual(('cosmeticType1',), collection)

class Method_get_certificates_attributes_as_tuples_Should(unittest.TestCase):
    def test_get_certificates_attributes_as_tuples_succesfully(self):
        # Arrange
        entry_cfk_list = EntryCFKList([EntryCFK('Brand Name', True, ['certificate1'], [])])

        # Act
        collection = entry_cfk_list.get_certificates_attributes_as_tuples()
        
        # Assert
        self.assertEqual((('certificate1',),), collection)

class Method_get_unique_certificates_attributes_Should(unittest.TestCase):
    def test_get_unique_certificates_attributes_succesfully(self):
        # Arrange
        entry_cfk_list = EntryCFKList([EntryCFK('Brand Name', True, ['certificate1', 'certificate1'], [])])

        # Act
        collection = entry_cfk_list.get_unique_certificates_attributes()
        
        # Assert
        self.assertEqual(('certificate1',), collection)

class Method_get_certified_items_Should(unittest.TestCase):
    def test_get_certified_items_succesfully(self):
        # Arrange
        
        e = EntryCFK('Brand Name', True, ['certificate1', 'certificate1'], [])
        e.brand_id = 1
        entry_cfk_list = EntryCFKList([e])

        # Act
        collection = entry_cfk_list.get_certified_items()
        
        # Assert
        self.assertEqual(((1,),), collection)

    def test_get_certified_items_succesfullyNoItems(self):
        # Arrange
        
        e = EntryCFK('Brand Name', False, [], ['cosmeticType1'])
        e.brand_id = 1
        entry_cfk_list = EntryCFKList([e])

        # Act
        collection = entry_cfk_list.get_certified_items()
        
        # Assert
        self.assertEqual((), collection)

class Method_get_not_certified_items_Should(unittest.TestCase):
    def test_get_not_certified_items_succesfully(self):
        # Arrange
        
        e = EntryCFK('Brand Name', False, [], ['cosmeticType1'])
        e.brand_id = 1
        entry_cfk_list = EntryCFKList([e])

        # Act
        collection = entry_cfk_list.get_not_certified_items()
        
        # Assert
        self.assertEqual(((1,),), collection)
    
    def test_get_not_certified_items_succesfullyNoItems(self):
        # Arrange
        
        e = EntryCFK('Brand Name', True, ['certificate1'], [])
        e.brand_id = 1
        entry_cfk_list = EntryCFKList([e])

        # Act
        collection = entry_cfk_list.get_not_certified_items()
        
        # Assert
        self.assertEqual((), collection)

class Method_add_brands_ids_Should(unittest.TestCase):
    def test_add_brands_ids_succesfully(self):
        # Arrange
        entry_cfk_list = EntryCFKList([
            EntryCFK('Brand Name1', False, [], ['cosmeticType1']), 
            EntryCFK('Brand Name2', True, ['certificate1'], [])
        ])

        # Act
        entry_cfk_list.add_brands_ids([(1, 'Brand Name1'), (2, 'Brand Name2')])
        
        # Assert
        self.assertEqual([1, 2], [entry_cfk_list.collection[0].brand_id, entry_cfk_list.collection[1].brand_id])
    
    def test_add_brands_ids_succesfully_NoMatch(self):
        # Arrange
        entry_cfk_list = EntryCFKList([
            EntryCFK('Brand Name1', False, [], ['cosmeticType1']), 
            EntryCFK('Brand Name2', True, ['certificate1'], [])
        ])

        # Act
        entry_cfk_list.add_brands_ids([(1, 'Brand Name3'), (2, 'Brand Name4')])
        
        # Assert
        self.assertEqual([None, None], [entry_cfk_list.collection[0].brand_id, entry_cfk_list.collection[1].brand_id])

class Method_get_brands_with_types_Should(unittest.TestCase):
    def test_get_brands_with_types_succesfully(self):
        # Arrange
        entry_cfk_list = EntryCFKList([
            EntryCFK('Brand Name1', False, [], ['cosmeticType1']), 
            EntryCFK('Brand Name2', False, [], ['cosmeticType1', 'cosmeticType2'])
        ])
        entry_cfk_list.collection[0].brand_id = 1
        entry_cfk_list.collection[1].brand_id = 2

        # Act
        collection = entry_cfk_list.get_brands_with_types()
        
        # Assert
        self.assertEqual([(1, 'cosmeticType1'), (2, 'cosmeticType1'), (2, 'cosmeticType2')], collection)

    def test_get_brands_with_types_returnEmpty(self):
        # Arrange
        entry_cfk_list = EntryCFKList([
            EntryCFK('Brand Name1', False, [], ['cosmeticType1']), 
            EntryCFK('Brand Name2', False, [], ['cosmeticType1', 'cosmeticType2'])
        ])
        entry_cfk_list.collection[0].brand_id = None
        entry_cfk_list.collection[1].brand_id = None

        # Act
        collection = entry_cfk_list.get_brands_with_types()
        
        # Assert
        self.assertEqual([], collection)

class Method_get_brands_certificates_attributes_Should(unittest.TestCase):
    def test_get_brands_certificates_attributes_succesfully(self):
        # Arrange
        entry_cfk_list = EntryCFKList([
            EntryCFK('Brand Name1', False, ['certificate1'], []), 
            EntryCFK('Brand Name2', False, ['certificate1', 'certificate2'], [])
        ])
        entry_cfk_list.collection[0].brand_id = 1
        entry_cfk_list.collection[1].brand_id = 2

        # Act
        collection = entry_cfk_list.get_brands_certificates_attributes()
        
        # Assert
        self.assertEqual([(1, 'certificate1'), (2, 'certificate1'), (2, 'certificate2')], collection)
    
    def test_get_brands_certificates_attributes_returnEmpty(self):
        # Arrange
        entry_cfk_list = EntryCFKList([
            EntryCFK('Brand Name1', False, ['certificate1'], []), 
            EntryCFK('Brand Name2', False, ['certificate1', 'certificate2'], [])
        ])
        entry_cfk_list.collection[0].brand_id = None
        entry_cfk_list.collection[1].brand_id = None

        # Act
        collection = entry_cfk_list.get_brands_certificates_attributes()
        
        # Assert
        self.assertEqual([], collection)
