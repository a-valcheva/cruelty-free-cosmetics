import unittest

from models.entry_cfk import EntryCFK


class Constructor_Should(unittest.TestCase):
    def test_sets_attributes(self):
        # Arrange & Act
        entry_cfk = EntryCFK('Brand Name', True, ['certificate1'], [])

        # Assert
        self.assertEqual('Brand Name', entry_cfk.name)
        self.assertEqual(True, entry_cfk.is_cf)
        self.assertEqual(['certificate1'], entry_cfk.certificates_attributes)
        self.assertEqual([], entry_cfk.cosmetics_types)
        self.assertEqual(None, entry_cfk.brand_id)

    def test_fail_with_valueerror_emptyName(self):
        with self.assertRaises(ValueError):
            e = EntryCFK('', True, ['certificate1'], [])
    
    def test_fail_with_valueerror_nameNone(self):
        with self.assertRaises(ValueError):
            e = EntryCFK('', True, ['certificate1'], [])

class NameSetter_Should(unittest.TestCase):
    def test_fail_with_valueerror_emptyName(self):
        with self.assertRaises(ValueError):
            e = EntryCFK('Brand Name', True, ['certificate1'], [])
            e.name = ''

    def test_fail_with_valueerror_nameNone(self):
        with self.assertRaises(ValueError):
            e = EntryCFK('Brand Name', True, ['certificate1'], [])
            e.name = None

class Get_brand_id_types_Should(unittest.TestCase):
    def test_get_brand_id_types_returnListOfTuples(self):
        # Arrange
        e = EntryCFK('Brand Name', True, [], ['cosmeticType1'])
        e.brand_id = 1

        # Act
        collection = e.get_brand_id_types()

        # Assert
        self.assertEqual([(1, 'cosmeticType1')], collection)

    def test_get_brand_id_types_returnNone(self):
        # Arrange
        e = EntryCFK('Brand Name', True, [], [])
        e.brand_id = 1

        # Act
        collection = e.get_brand_id_types()

        # Assert
        self.assertEqual(None, collection)

class Get_brand_id_certificats_Should(unittest.TestCase):
    def test_get_brand_id_certificats_returnListOfTuples(self):
        # Arrange
        e = EntryCFK('Brand Name', True, ['certificate1'], [])
        e.brand_id = 1

        # Act
        collection = e.get_brand_id_certificats()

        # Assert
        self.assertEqual([(1, 'certificate1')], collection)

    def test_get_brand_id_certificats_returnNone(self):
        # Arrange
        e = EntryCFK('Brand Name', True, [], [])
        e.brand_id = 1

        # Act
        collection = e.get_brand_id_certificats()

        # Assert
        self.assertEqual(None, collection)