from itemloaders.processors import TakeFirst, MapCompose, Compose, Identity
from scrapy.loader import ItemLoader

# def collections_processor(value):
#     return value or []

class CrueltyFreeKittyItemLoader(ItemLoader):
    default_output_processor = TakeFirst()
    is_cf_in = MapCompose(lambda x: x == 'Cruelty-free')
    name_in = MapCompose(str.strip)
    # is_cf_in = Compose(lambda x: x == 'Cruelty-free')
    # name_in = Compose(str.strip)
    cosmetics_types_in = MapCompose(lambda x: x.strip())
    cosmetics_types_out = Identity()
    certificates_attributes_in = MapCompose(lambda x: x.strip())
    certificates_attributes_out = Identity()

# class LeapingBunnyItemLoader(ItemLoader):
#     default_output_processor = TakeFirst()
#     name_in = MapCompose(str.strip)
#     cosmetics_types_in = MapCompose(lambda x: x.strip())
#     cosmetics_types_out = Identity()
