import scrapy
from cruelty_free_cosmetics.items import CrueltyFreeKittyItem
from cruelty_free_cosmetics.itemloaders import CrueltyFreeKittyItemLoader


class CrueltyFreeKittySpider(scrapy.Spider):
    name = "cruelty_free_kitty"
    allowed_domains = ["www.crueltyfreekitty.com"]
    start_urls = [
        'https://www.crueltyfreekitty.com/companies-that-test-on-animals/',
        'https://www.crueltyfreekitty.com/list-of-cruelty-free-brands/'
    ]

    custom_settings = {
        'ITEM_PIPELINES': {
            "cruelty_free_cosmetics.pipelines.MySQLPipeline": 300,
        }
    }

    def parse(self, response):
        for cfk_i in response.xpath('.//div[@class="brand-list__list__item"]'):
            cfk_item = CrueltyFreeKittyItemLoader(item=CrueltyFreeKittyItem(), selector=cfk_i)
            cfk_item.add_xpath('name', './/a[@class="heading heading--secondary brand-list__list__title"]/text()')
            cfk_item.add_xpath('cosmetics_types', './/div[@class="brand-list__list__category-wrapper--desktop"]/span/text()')
            cfk_item.add_xpath('certificates_attributes', './/div[@class="brand-list__list__tag-container"]/span/@data-tippy-content')
            cfk_item.add_xpath('is_cf', './/span[contains(concat(" ", normalize-space(@class), " "), "round-tag round-tag--secondary round-tag--not-cruelty-free brand-list__list__status") or contains(concat(" ", normalize-space(@class), " "), "round-tag round-tag--secondary round-tag--cruelty-free brand-list__list__status")]/@data-tippy-content')
            yield cfk_item.load_item()
