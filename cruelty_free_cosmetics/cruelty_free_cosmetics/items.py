# Define here the models for your scraped items
#
# See documentation in:
# https://docs.scrapy.org/en/latest/topics/items.html

# import scrapy
from scrapy import Item, Field


class BrandBaseItem(Item):
    name = Field()
    cosmetics_types = Field()

class CrueltyFreeKittyItem(BrandBaseItem):
    certificates_attributes = Field()
    shops = Field()
    is_cf = Field()

# class CrueltyFreeInternationalItem(BrandBaseItem):
#     resume = Field()
#     regions = Field()
#     website = Field()

# class LeapingBunnyItem(BrandBaseItem):
#     website = Field()
#     regions = Field()
#     certificate_year = Field()
#     description = Field()
#     availability_list = Field()
#     has_logo = Field()