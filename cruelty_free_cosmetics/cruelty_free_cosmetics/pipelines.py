# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://docs.scrapy.org/en/latest/topics/item-pipeline.html


# useful for handling different item types with a single interface
from itemadapter import ItemAdapter

# from models.entry_cfk import EntryCFK
from models.entry_cfk_list import EntryCFKList, EntryCFK
from common.credentials import get_credentials
from db_mysql.db_mysql import create_connection
from db_mysql.cfk_data import cfk_store_db
from db_mysql.backup_mysql_db import backup_db


# class CrueltyFreeCosmeticsPipeline:
#     def process_item(self, item, spider):
#         return item


class MySQLPipeline(object):

    def __init__(self):
        self._cfk_items = EntryCFKList()
        self.credentials = get_credentials('common/mysql_credentials.txt')
        self.connection = create_connection(self.credentials)
        self.backup_database()
        

    def backup_database(self):
        backup_db(
            host = self.credentials[0],
            user = self.credentials[1],
            password = self.credentials[2],
            database = self.credentials[3],
        )

    def process_item(self, item, spider):
        item.setdefault('certificates_attributes', [])
        item.setdefault('cosmetics_types', [])
        if item['name'] and item['is_cf'] is not None:
            temp_item = EntryCFK(item['name'], item['is_cf'], item['certificates_attributes'], item['cosmetics_types'])
            self._cfk_items.add_item(temp_item)

        if self._cfk_items.lendth >= 500:
            cfk_store_db(self.connection, self._cfk_items)
            self._cfk_items = EntryCFKList()

        #we need to return the item below as Scrapy expects us to!
        return item

    def close_spider(self, spider):
        if self._cfk_items:
            cfk_store_db(self.connection, self._cfk_items)
            print(f'Collection length: {self._cfk_items.lendth}')
            self._cfk_items = EntryCFKList()
            print(f'Collection length: {self._cfk_items.lendth}')
        self.connection.close()
