-- MySQL dump 10.13  Distrib 8.0.32, for Win64 (x86_64)
--
-- Host: localhost    Database: cosmetics_cf
-- ------------------------------------------------------
-- Server version	8.0.32

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `accreditors`
--

DROP TABLE IF EXISTS `accreditors`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `accreditors` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `abbreviation` varchar(45) NOT NULL,
  `start_url` varchar(45) NOT NULL,
  `domain` varchar(45) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name_UNIQUE` (`name`),
  UNIQUE KEY `abbreviation_UNIQUE` (`abbreviation`),
  UNIQUE KEY `start_url_UNIQUE` (`start_url`),
  UNIQUE KEY `domain_UNIQUE` (`domain`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `accreditors`
--

LOCK TABLES `accreditors` WRITE;
/*!40000 ALTER TABLE `accreditors` DISABLE KEYS */;
/*!40000 ALTER TABLE `accreditors` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `brands`
--

DROP TABLE IF EXISTS `brands`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `brands` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `brand_name_UNIQUE` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=3000 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `brands`
--

LOCK TABLES `brands` WRITE;
/*!40000 ALTER TABLE `brands` DISABLE KEYS */;
INSERT INTO `brands` VALUES (269,'100% Pure'),(270,'14e Cosmetics'),(271,'35 Thousand'),(1,'3CE'),(272,'3INA'),(273,'54 Thrones'),(274,'A.P. Chem'),(275,'Abba'),(276,'About-Face'),(2,'Acqua Di Parma'),(277,'Acure'),(278,'Adorn Cosmetics'),(279,'Adwoa Beauty'),(280,'Aesop'),(281,'AF94'),(282,'Afterglow Cosmetics'),(283,'AG Hair'),(284,'Aila Cosmetics'),(285,'Alaffia'),(286,'Alba Botanica'),(287,'Alder New York'),(3,'Algenist'),(288,'Alima Pure'),(289,'Alleyoop'),(4,'Almay'),(290,'Alpha-H'),(291,'alphée'),(292,'Alpyn Beauty'),(293,'Altruist'),(5,'Always'),(294,'Amâz Active Skincare'),(6,'American Crew'),(295,'Amika'),(7,'AmorePacific'),(8,'Amway'),(296,'Anastasia Beverly Hills'),(297,'Andalou Naturals'),(9,'Anna Sui'),(298,'Annabelle'),(299,'Anomaly'),(300,'Anthony'),(301,'Antipodes'),(302,'Antonym'),(10,'Aquafresh'),(11,'Aquaphor'),(12,'Aramis'),(303,'ARCONA'),(304,'Arctic Fox'),(305,'Ardell'),(306,'Ardency Inn'),(13,'Arm & Hammer'),(14,'Armani Beauty'),(307,'Aromatica'),(308,'Arquiste'),(309,'As I Am'),(310,'AspenClean'),(311,'Astonish'),(15,'Atelier Cologne'),(312,'Athar’a Pure'),(313,'Athr Beauty'),(314,'Attitude'),(315,'Au Naturale'),(316,'Aubrey Organics'),(317,'Auric'),(318,'Auromere'),(319,'Aussie'),(320,'Australian Gold'),(321,'Australis'),(322,'Avalea'),(323,'Avalon Organics'),(324,'Aveda'),(16,'Aveeno'),(17,'Avene'),(325,'Averr Aglow'),(18,'Avon'),(19,'Axe'),(326,'Axiology'),(327,'AXIS-Y'),(328,'Babo Botanicals'),(329,'Badger'),(330,'BaeBlu'),(20,'Bain de Soleil'),(21,'Balenciaga'),(331,'Bali Body'),(332,'Bali Lash'),(333,'Balm Balm'),(22,'Banana Boat'),(23,'Banila Co'),(334,'barefoot SCIENTIST'),(335,'bareLUXE Skincare'),(336,'bareMinerals'),(337,'Barry M'),(338,'basd'),(24,'Batiste'),(339,'Be Kind Beauty'),(340,'Beast'),(341,'Beauty Favours'),(342,'Beauty of Joseon'),(343,'Beauty Without Cruelty'),(344,'BeautyBio'),(345,'Beautyblender'),(346,'BeautyStat'),(347,'Becca'),(25,'Bed Head'),(348,'Beekman 1802'),(26,'Belif'),(349,'Bella Vida Santa Barbara'),(350,'Bellapierre'),(351,'Ben Nye'),(352,'Benecos'),(27,'Benefit'),(353,'Benton'),(354,'Besame'),(355,'Better & Better'),(356,'Better Not Younger'),(357,'BH Cosmetics'),(28,'Bic'),(358,'Billie Eilish Fragrances'),(359,'Billy Jealousy'),(360,'bioClarity'),(29,'Bioderma'),(30,'Bioeffect'),(361,'Biolage'),(31,'Biologique Recherche'),(32,'Biore'),(362,'BioSilk'),(363,'Biossance'),(33,'Biotherm'),(364,'Bite Beauty'),(365,'Black Radiance'),(366,'Blinc'),(367,'Bliss'),(368,'Blissoma'),(34,'Blistex'),(369,'BLK/OPL'),(370,'Blume'),(35,'Bobbi Brown'),(371,'Bomb Cosmetics'),(372,'Bondi Sands'),(373,'Booda Organics'),(374,'Boots'),(36,'Borghese'),(375,'Boscia'),(376,'Bossy Cosmetics'),(377,'Botana'),(378,'Botanic Tree'),(379,'Botanics'),(380,'Boulder Clean'),(37,'Bounce'),(38,'Bourjois'),(381,'Brandefy Skin'),(382,'Bread Beauty Supply'),(383,'Briogeo'),(384,'BUBBLE Skincare'),(385,'Bulldog'),(386,'Bumble and Bumble'),(39,'Burberry'),(387,'Burt’s Bees'),(388,'buttah.'),(389,'butter LONDON'),(390,'Buxom'),(40,'BVLGARI'),(391,'By Beauty Bay'),(392,'By Rosie Jane'),(41,'By Terry'),(393,'By Wishtrend'),(394,'BYBI Beauty'),(395,'Byoma'),(42,'Byredo'),(396,'C.O. Bigelow'),(43,'Cacharel'),(397,'Cailyn Cosmetics'),(398,'Cake'),(44,'Calgon'),(399,'California Tan'),(400,'Caliray'),(45,'Calvin Klein'),(401,'Captain Blankenship'),(402,'Carbon Theory'),(46,'Carefree'),(47,'Caress'),(403,'Cargo'),(404,'Carol’s Daughter'),(48,'Carolina Herrera'),(405,'Caswell-Massey'),(406,'Cate McNabb'),(407,'Catrice'),(49,'Caudalie'),(408,'Cela'),(409,'Celeb Luxury'),(410,'Ceramedx'),(50,'CeraVe'),(411,'Ceremonia'),(412,'Certain Dri'),(51,'Cetaphil'),(52,'Chanel'),(413,'Chantecaille'),(53,'Chapstick'),(414,'Charlotte Tilbury'),(415,'Cheekbone Beauty'),(416,'CHI'),(417,'Chi Chi Cosmetics'),(418,'Chiki Buttah'),(419,'China Glaze'),(54,'Chloe'),(55,'Christina Aguilera'),(420,'Christophe Robin'),(421,'Ciaté'),(422,'City Beauty'),(56,'Clairol'),(57,'Clarins'),(58,'Clarisonic'),(423,'CLE Cosmetics'),(59,'Cle de Peau'),(424,'Clean'),(60,'Clean & Clear'),(425,'Cleancult'),(426,'Cleanwell'),(61,'Clear'),(62,'Clearasil'),(427,'Cleo+Coco'),(63,'Clinique'),(64,'Clorox'),(428,'Clove + Hallow'),(65,'CND'),(429,'Coco & Eve'),(430,'Cocofloss'),(431,'Cocokind'),(432,'Coconut Matter'),(66,'Colgate'),(433,'Color Dept.'),(434,'Coloured Raine'),(435,'Colourpop'),(67,'Comet'),(436,'Community Sixty Six'),(437,'Conair'),(438,'Concrete Minerals'),(439,'Coola'),(68,'Coppertone'),(440,'Copycat Beauty'),(441,'Cosmedix'),(442,'Cosmoss'),(443,'COSRX'),(69,'Coty'),(444,'Cover FX'),(445,'Covergirl'),(70,'Crabtree & Evelyn'),(446,'Crazy Color'),(447,'Crazy Rumors'),(448,'Cremo'),(449,'Crépe Erase'),(71,'Crest'),(450,'Crystal'),(72,'Curel'),(451,'Curlsmith'),(452,'Curology'),(453,'D. S. & Durga'),(454,'Dae'),(455,'DAMDAM'),(456,'Danessa Myricks'),(457,'Das Boom'),(73,'Davidoff'),(458,'Davids'),(74,'Dawn'),(459,'De Mamiel'),(460,'Dear, Klairs'),(461,'Deborah Lippmann'),(462,'Deck of Scarlet'),(75,'Decléor'),(463,'Deco Miami'),(464,'Deep Steep'),(465,'DefineMe Fragrance'),(76,'Degree'),(466,'Delicate Daisys Botanical Beauty'),(467,'delilah'),(77,'Demeter'),(468,'Derma E'),(469,'Dermablend'),(470,'DERMAdoctor'),(471,'Dermalogica'),(472,'DermOrganic'),(473,'Desert Essence'),(474,'DevaCurl'),(475,'DeVita'),(476,'Dezi Skin'),(78,'DHC'),(477,'Dial'),(79,'Diesel'),(80,'Differin'),(478,'Dinobi'),(479,'Dionis'),(81,'Dior'),(82,'Dolce & Gabbana'),(480,'Doll 10'),(481,'dome BEAUTY'),(83,'Donna Karan'),(482,'Dose Of Colors'),(483,'Dotted Zebra'),(484,'Double Dare'),(485,'Dove'),(84,'Downy'),(486,'dpHUE'),(487,'Dr Teal’s'),(488,'Dr. Alkaitis'),(489,'Dr. Barbara Sturm'),(85,'Dr. Brandt'),(490,'Dr. Bronner’s'),(491,'Dr. Dennis Gross'),(492,'Dr. Hauschka'),(86,'Dr. Jart+'),(493,'Dr. Loretta'),(494,'Dr. PAWPAW'),(495,'Drunk Elephant'),(496,'Drybar'),(87,'Dunhill'),(497,'DuWop'),(498,'Earth Breeze'),(499,'Earth Harbor'),(500,'Earth Tu Face'),(501,'Earth’s Beauty'),(502,'Earths Laundry'),(503,'EasterMia'),(504,'Eau de Juice'),(505,'Ecco Bella'),(506,'Eco Lips'),(507,'Eco Me'),(508,'EcoColors'),(509,'ECOS'),(510,'EcoTools'),(511,'Ecover'),(512,'Eczema Honey'),(513,'Edible Beauty'),(514,'Edward Bess'),(515,'elaluz'),(516,'Elate'),(88,'Elemis'),(517,'Eleven Australia'),(518,'elf'),(89,'Elie Saab'),(90,'Elizabeth Arden'),(519,'Elizabeth Grant'),(520,'Ella + Mila'),(521,'Ellis Faas'),(522,'Ellovi'),(91,'Elta MD'),(523,'Em Cosmetics'),(524,'Emani'),(525,'Embryolisse'),(526,'Eminence Organic Skincare'),(527,'EO Products'),(528,'EOS'),(529,'Epic Blend'),(530,'Ere Perez'),(92,'Erno Laszlo'),(93,'Escada'),(531,'Essence'),(532,'Estate Cosmetics'),(94,'Estée Lauder'),(533,'Ethique'),(95,'Etude House'),(96,'Eucerin'),(534,'Eva Nyc'),(535,'evanhealy'),(97,'Eve Lom'),(536,'Everyday Minerals'),(537,'everyone'),(538,'Evolve Organic Beauty'),(539,'EVOLVh'),(540,'Exa Beauty'),(541,'Eyeko'),(542,'Fable & Mane'),(543,'Face Atelier'),(544,'Facetheory'),(545,'Faith In Nature'),(546,'Farmacy Beauty'),(547,'Farsali'),(548,'Fat and the Moon'),(98,'Febreze'),(549,'Fekkai'),(99,'Femfresh'),(100,'Fendi'),(550,'Fenty Beauty'),(551,'Fenty Skin'),(101,'Filorga'),(552,'Finding Ferdinand'),(553,'Fiona Stiles'),(554,'Fior Minerals'),(555,'First Aid Beauty'),(556,'Fitglow Beauty'),(557,'Flamingo'),(558,'Flawless Lashes By Loreta'),(559,'Fleur & Bee'),(560,'Flora + Bast'),(561,'Floral Street'),(562,'Florence by Mills'),(563,'Floris London'),(564,'Flower'),(565,'Follain'),(566,'Forager Botanicals'),(567,'Formula 10.0.6'),(568,'Formulary 55'),(569,'Frank Body'),(570,'Freck Beauty'),(102,'Freeman Beauty'),(571,'French Girl'),(103,'Fresh'),(572,'Function of Beauty'),(573,'Fur'),(574,'Furless'),(575,'Gabriel Cosmetics'),(104,'Gain'),(576,'Garnier'),(577,'Gerard Cosmetics'),(578,'GiGi'),(105,'Gillette'),(106,'Giorgio Armani'),(579,'Giovanni'),(580,'Girlactik'),(581,'Gisou'),(107,'Givenchy'),(108,'Glade'),(109,'GLAMGLOW'),(582,'Glo Skin Beauty'),(583,'Gloss Moderne'),(584,'Glossier'),(585,'Glow Jar Beauty'),(586,'Glow Recipe'),(587,'Gold Bond'),(110,'Goldwell'),(588,'Good Dye Young'),(589,'good light'),(590,'Good Molecules'),(591,'Goodness'),(592,'goPure Beauty'),(593,'GOSH'),(111,'Got2b'),(594,'Grande Cosmetics'),(595,'Graydon'),(596,'Green Label Goods'),(112,'Green Works'),(597,'Gressa'),(598,'Grounded Sage'),(599,'Grown Alchemist'),(113,'Gucci'),(114,'Guerlain'),(600,'GXVE'),(601,'Gypsy Women Soap Co.'),(602,'Habit'),(115,'Hada Labo'),(116,'Hair Food'),(603,'Hairitage by Mindy'),(604,'Hairstory'),(605,'Hally'),(606,'HAN'),(607,'Hana Organic Skincare'),(608,'Hard Candy'),(609,'Harlem Botanica'),(610,'Harry’s'),(611,'Harvey Prince Organics'),(612,'Hask'),(613,'Haus Laboratories'),(614,'Hawaiian Tropic'),(117,'Head & Shoulders'),(118,'Helena Rubinstein'),(615,'Hello Products'),(616,'Hempme'),(617,'Hempz'),(119,'Henkel'),(618,'Herbal Essences'),(619,'Herbatint'),(620,'Herbivore Botanicals'),(621,'Hero Cosmetics'),(622,'Honore Des Pres'),(623,'Hourglass'),(624,'House of Lashes'),(625,'Huda Beauty'),(120,'Hugo Boss'),(626,'Hurraw!'),(627,'Huygens'),(628,'Hynt Beauty'),(629,'I Am Selfcare'),(630,'I’m From'),(631,'IGK'),(632,'Il Makiage'),(633,'Iles Formula'),(634,'ILIA'),(635,'Illamasqua'),(636,'Indie Lee'),(637,'Infuse by Casabella'),(638,'Inglot'),(639,'Inika'),(640,'Innersense'),(641,'INNERSOUL'),(121,'Innisfree'),(642,'InstaNatural'),(122,'Irish Spring'),(123,'Isdin'),(643,'Isle of Paradise'),(644,'Isntree'),(124,'Issey Miyake'),(645,'IT Cosmetics'),(646,'It’s A 10'),(647,'iUNIK'),(125,'Ivory'),(648,'Jacq’s'),(649,'Jane'),(650,'Jane Iredale'),(651,'Japonesque'),(652,'JASON'),(653,'Jason Wu Beauty'),(654,'Jecca Blac'),(655,'Jeffree Star'),(126,'Jergens'),(127,'Jimmy Choo'),(656,'JINsoon'),(657,'JLo Beauty'),(128,'Jo Malone'),(129,'John Frieda'),(658,'John Masters Organics'),(130,'John Varvatos'),(131,'Johnson & Johnson'),(659,'Joico'),(660,'Jolie Vegan'),(661,'Jones Road Beauty'),(662,'Jordana'),(663,'Josh Rosebrook'),(664,'Josie Maran'),(665,'Jouer'),(132,'Joy'),(666,'Juice Beauty'),(667,'Julep'),(133,'Jurlique'),(668,'Juvia’s Place'),(669,'JVN Hair'),(670,'K18 Hair'),(134,'Kaboom'),(671,'Kadalys'),(672,'Kahina Giving Beauty'),(673,'Kaia Naturals'),(674,'Kaja Beauty'),(675,'Kari Gran'),(676,'Kate McLeod'),(677,'Kate Somerville'),(678,'Kayali'),(679,'Kaylaan'),(680,'Kear'),(681,'Kenra'),(135,'Kenzo'),(136,'Kerastase'),(682,'Kesha Rose Beauty'),(683,'Kester Black'),(684,'Kevin Murphy'),(137,'Kiehl’s'),(685,'KIKO Milano'),(686,'Kind Laundry'),(687,'KINDri Los Angeles'),(688,'Kinship'),(689,'Kiss My Face'),(690,'Kjaer Weis'),(691,'KKW Beauty'),(138,'Klorane'),(692,'Koh Gen Do'),(693,'Kopari Beauty'),(694,'Kora Organics'),(695,'Korres'),(696,'Kosas'),(139,'Kose'),(140,'Kotex'),(697,'Krave Beauty'),(698,'Kreyol Essence'),(699,'Krī Skincare'),(700,'Kristin Ess'),(701,'Kryolan'),(702,'Kure Bazaar'),(703,'KVD Vegan Beauty'),(704,'Kylie Cosmetics'),(705,'Kylie Skin'),(706,'Kypris'),(141,'L’Occitane'),(142,'L’Oréal'),(707,'LA Colors'),(708,'La Fleur by Livvy'),(709,'LA Girl'),(143,'La Mer'),(144,'La Roche-Posay'),(145,'Lab Series For Men'),(146,'Lacoste'),(147,'Lady Speed Stick'),(148,'Lancaster Beauty'),(710,'Lancer'),(149,'Lancôme'),(150,'Laneige'),(711,'Lano'),(151,'Lanvin'),(712,'Lather'),(713,'Laura Geller'),(714,'Lauren B. Beauty'),(715,'Lavanila'),(716,'Lawless'),(717,'Le Couvent Des Minimes'),(718,'Le Labo'),(719,'Le Petit Olivier'),(720,'Level Naturals'),(721,'LH Cosmetics'),(722,'Lilah B.'),(723,'Lily Lolo'),(724,'LilyAna Naturals'),(725,'Lime Crime'),(726,'Lina Hanson'),(152,'Listerine'),(727,'Lit Cosmetics'),(728,'Little Seed Farm'),(729,'Live Clean'),(730,'Live Tinted'),(731,'Living Proof'),(732,'Liz Earle'),(733,'LolaVie'),(734,'Loli Beauty'),(735,'Lolita Lempicka'),(736,'Lotus Wei'),(737,'Love Sun Body'),(738,'Lovefresh'),(739,'Lovesong Beauty'),(153,'Lubriderm'),(740,'Lulu Organics'),(741,'Lunar Beauty'),(742,'Lurk'),(743,'LUSH'),(154,'Lux'),(744,'LVX'),(745,'Lys Beauty'),(155,'Lysol'),(156,'MAC'),(746,'Mad Hippie'),(747,'Madara Cosmetics'),(748,'Madison Reed'),(749,'Maëlys Cosmetics'),(750,'MAHALO'),(751,'Maison Louis Marie'),(157,'Make Up For Ever'),(752,'Makeup By Mario'),(753,'Makeup Geek'),(754,'Makeup Revolution'),(158,'Mamonde'),(755,'Manic Panic'),(756,'Marc Jacobs Beauty'),(159,'Marc Jacobs Fragrances'),(757,'Marcelle'),(758,'Maria Nila'),(759,'Marie Hunter Beauty'),(760,'Mario Badescu'),(761,'Marks & Spencer'),(762,'Marula'),(160,'Mary Kay'),(161,'Matrix'),(162,'Max Factor'),(763,'May Lindstrom'),(163,'Maybelline'),(764,'Mayraki Professional'),(765,'ME Cosmetics'),(766,'Mehron'),(767,'Melanin Haircare'),(768,'Melody’s Magical Scents'),(769,'Melt Cosmetics'),(770,'Mented Cosmetics'),(771,'Meow Meow Tweet'),(772,'Merit'),(773,'Merle Norman'),(774,'Method'),(164,'Michael Kors'),(775,'Milani'),(776,'Milk Makeup'),(165,'Miller Harris'),(777,'Mineral Fusion'),(778,'Miracle Made'),(166,'Missha'),(167,'Mitchum'),(168,'Miu Miu'),(779,'Molly’s Suds'),(780,'Molton Brown'),(781,'MONDAY'),(782,'Moroccanoil'),(169,'Mr. Clean'),(783,'Mrs. Meyer’s'),(170,'Muji'),(784,'Mukti'),(785,'MUN'),(786,'Murad'),(787,'Muri Lelu'),(788,'MV Skintherapy'),(789,'My Daughter Fragrances'),(790,'MyChelle'),(791,'Nabla Cosmetics'),(792,'Nad’s'),(171,'Nair'),(793,'Nani Pua'),(172,'NARS'),(794,'Natasha Denona'),(795,'Natio'),(796,'Native'),(797,'Natura'),(798,'Natura Siberica'),(173,'Natural Instincts'),(799,'Nature Clean'),(800,'Nature’s Gate'),(801,'NCLA'),(174,'Necessaire'),(802,'Nellie’s'),(175,'NEOGEN'),(803,'NEST Fragrances'),(176,'Neutrogena'),(177,'Nexxus'),(178,'Nice’n Easy'),(179,'Nina Ricci'),(180,'Nioxin'),(804,'Nip + Fab'),(181,'Nivea'),(805,'No B.S.'),(806,'No7'),(807,'Not Your Mother’s'),(808,'Noughty'),(809,'Nourish Organic'),(182,'Noxzema'),(810,'Nubian Heritage'),(811,'Nude By Nature'),(812,'Nude Skincare'),(183,'Nuxe'),(184,'NYC'),(813,'nyl'),(814,'NYX'),(185,'o.b. Tampons'),(186,'O.TWO.O Cosmetics'),(187,'Obagi'),(815,'Odacité'),(816,'Odylique'),(817,'OFRA Cosmetics'),(188,'OGX'),(818,'Oil Divine'),(189,'Ojon'),(819,'Olaplex'),(190,'Olay'),(191,'Old English'),(192,'Old Spice'),(820,'Ole Henriksen'),(821,'Omayma Skin'),(822,'Omorovicza'),(823,'One Love Organics'),(824,'One/Size'),(825,'OOO Polish'),(193,'OPI'),(194,'Oral B'),(826,'Organic Bath Co'),(827,'Oribe'),(195,'Oriflame'),(196,'Origins'),(828,'Orly'),(829,'OSEA Malibu'),(830,'Oshun Organics'),(831,'Ouai'),(832,'Oui the People'),(833,'Ouidad'),(834,'Out of Africa'),(835,'oVertone'),(197,'OxiClean'),(836,'OZ Naturals'),(837,'Pacifica'),(198,'Paco Rabanne'),(838,'Pai'),(839,'Painted Earth'),(840,'Palladio'),(199,'Palmer’s'),(200,'Palmolive'),(841,'Pangea Organics'),(201,'Pantene'),(842,'ParadiseGal'),(843,'Parissa'),(202,'Pat McGrath Labs'),(844,'Patchology'),(845,'Patrick Ta'),(846,'Paul Mitchell'),(847,'Paula’s Choice'),(848,'Peace Out'),(849,'Peach & Lily'),(850,'Peach Slices'),(851,'Penhaligon’s'),(852,'People Of Color Beauty'),(853,'Perfekt'),(854,'Perricone MD'),(203,'Peter Thomas Roth'),(855,'Phil Smith Be Gorgeous'),(204,'Philosophy'),(856,'Phlur'),(857,'Physicians Formula'),(205,'Phyto'),(206,'Pine-Sol'),(858,'Pink Chawkulit'),(859,'Pinrose'),(860,'PiperWai'),(861,'Pixi'),(862,'Plant Apothecary'),(863,'Pleni'),(207,'Ponds'),(864,'Pour Le Monde'),(208,'Prada'),(865,'Pravana'),(866,'Pretty Frank'),(867,'Primark'),(868,'Priti NYC'),(209,'Procter & Gamble'),(869,'Prose'),(870,'Proven Skincare'),(871,'Province Apothecary'),(872,'Pupa Milano'),(873,'PUR Cosmetics'),(874,'Puracy'),(875,'Pure & Gentle'),(876,'Pure Anada'),(877,'Pure Culture Beauty'),(878,'Pureology'),(210,'Purex'),(879,'PURITO'),(880,'Purlisse'),(881,'PYT Beauty'),(882,'QMBeautique'),(883,'Queen Helene'),(884,'Queen Street Apothecary'),(885,'Quo Beauty'),(886,'r.e.m. beauty'),(887,'Radical Skincare'),(888,'Rahua'),(211,'Ralph Lauren'),(889,'RANAVAT'),(890,'Range Beauty'),(891,'Rare Beauty'),(892,'Rare Elements'),(893,'Raw Sugar'),(894,'RCMA'),(895,'re:p'),(896,'Real Simple Clean'),(897,'Real Techniques'),(898,'Red Apple Lipstick'),(212,'Red Earth'),(213,'Redken'),(899,'REFY'),(214,'Rembrandt'),(900,'REN'),(901,'Renee Rouleau'),(902,'Reverie'),(903,'Revive7 Science'),(215,'Revlon'),(216,'Rexona'),(904,'rhode'),(905,'Rimmel London'),(906,'Rinna Beauty'),(907,'Rita Hazan'),(908,'Rituals'),(909,'Rituel De Fille'),(910,'RMS Beauty'),(217,'RoC'),(911,'Rodial'),(912,'Root Science'),(913,'Rose Inc'),(914,'Rosebud Perfume Company'),(915,'Rouge Bunny Rouge'),(916,'Rovectin'),(218,'S.C. Johnson'),(917,'S.W. Basics'),(918,'S’ABLE Labs'),(919,'Sachajuan'),(920,'Saie'),(921,'SAINT Cosmetics'),(922,'Saint Jane'),(219,'Sally Hansen'),(923,'Sand & Sky'),(924,'Sappho New Paradigm'),(925,'Saranghae'),(926,'Saturday Skin'),(220,'Schick'),(927,'Schmidt’s Naturals'),(221,'Schwartzkopf'),(222,'Scope'),(928,'Scotch Naturals'),(223,'Sebastian'),(224,'Sebastian Professional'),(929,'Seche'),(930,'SEEN'),(931,'Selfless by Hyram'),(225,'Sensodyne'),(226,'Sephora Collection'),(932,'Seventh Generation'),(227,'Sexy Hair'),(933,'Shani Darden'),(934,'SheaMoisture'),(228,'Shiseido'),(229,'Shu Uemura'),(935,'Sibu'),(936,'Sienna Byron Bay'),(937,'Silk Naturals'),(938,'Simple'),(939,'Simpli Skin MD'),(230,'Sinful Colors'),(940,'Sioris'),(231,'Sisley  Paris'),(232,'SK-II'),(233,'Skin Inc Supplement Bar'),(941,'Skin Laundry'),(942,'Skinblossom'),(234,'SkinCeuticals'),(943,'Skinfix'),(235,'Skinfood'),(944,'Skinny & Co'),(945,'Skylar'),(946,'skyn ICELAND'),(947,'Sleek'),(948,'Smashbox'),(949,'Smith & Cult'),(950,'Soap & Glory'),(951,'Soapwalla'),(236,'Soft & Dri'),(237,'Softsoap'),(952,'Sol de Janeiro'),(953,'Something More'),(238,'Son & Park'),(954,'Sonia Kashuk'),(955,'SpaRitual'),(956,'St. Ives'),(957,'St. Tropez'),(958,'Standard Self Care'),(959,'Starface'),(960,'Stargazer'),(239,'Stayfree'),(961,'Stella'),(962,'Stella McCartney'),(963,'Stellar Beauty'),(964,'Stila'),(965,'Strange Invisible'),(966,'strb.'),(967,'Studio Makeup'),(968,'Suave'),(969,'Sugarpill'),(970,'Suki'),(971,'Sukin'),(240,'Sulwhasoo'),(972,'Summer Fridays'),(973,'Sun Bum'),(974,'Sunday II Sunday'),(975,'Sunday Riley'),(241,'Sunlight'),(976,'SUNNIE Skin'),(242,'Sunsilk'),(977,'Suntegrity'),(978,'Super Deodorant'),(979,'Superdrug'),(980,'Supergoop!'),(981,'Supersmile'),(982,'Susteau'),(983,'Suva Beauty'),(984,'Sweet Chef'),(243,'Swiffer'),(985,'Tan-Luxe'),(986,'Tanologist'),(987,'Tarte'),(988,'Tata Harper'),(989,'TATCHA'),(990,'TEK'),(991,'Terra Beauty Bars'),(992,'Terrakai Skin'),(993,'The 7 Virtues'),(994,'The Beauty Chef'),(995,'The Body Shop'),(996,'The Creme Shop'),(244,'The Face Shop'),(997,'The Green Beaver Company'),(998,'The Honey Pot Co'),(999,'The Inkey List'),(1000,'The Ordinary'),(1001,'The Outset'),(1002,'The Quick Flick'),(1003,'The Route'),(1004,'The Seaweed Bath Co.'),(1005,'The Vegan Glow'),(1006,'theBalm'),(1007,'Then I Met You'),(1008,'Thesis'),(245,'Thierry Mugler'),(1009,'Thinkbaby'),(1010,'Thinksport'),(1011,'This is L.'),(1012,'Three Ships Beauty'),(1013,'Thrive Causemetics'),(246,'Tide'),(247,'Tigi'),(1014,'TO112'),(248,'Tom Ford'),(1015,'Tom’s of Maine'),(249,'Tommy Hilfiger'),(1016,'Tony Moly'),(250,'Too Cool For School'),(1017,'Too Faced'),(1018,'Tower 28'),(1019,'Trader Joe’s'),(1020,'Tree Hut'),(1021,'Tresemme'),(1022,'trèStiQue'),(1023,'Trilogy'),(1024,'Trish McEvoy'),(1025,'True Moringa'),(1026,'Trufora'),(1027,'Trust Fund Beauty'),(1028,'Tsi-La'),(1029,'Tula'),(1030,'Typology'),(1031,'Ulta Beauty'),(1032,'Umberto Giannini'),(1033,'Undone Beauty'),(1034,'Unicorn Gang Shop'),(251,'Unilever'),(1035,'Unpaste'),(1036,'UnSun'),(1037,'Uoma Beauty'),(1038,'UpCircle'),(1039,'Urban Decay'),(1040,'Urban Skin Rx'),(1041,'Urban Veda'),(1042,'URGO Beauty'),(1043,'Ursa Major'),(252,'V05'),(1044,'Vacation'),(253,'Valentino'),(1045,'Vamigas'),(1046,'Vapour Organic Beauty'),(1047,'Vasanti Cosmetics'),(254,'Vaseline'),(255,'Veet'),(1048,'Velour'),(256,'Venus'),(257,'Vera Wang'),(1049,'Verb'),(1050,'Vered Organic Botanicals'),(1051,'Vernon Francois'),(258,'Versace'),(1052,'Versed'),(1053,'Vesca Beauty'),(259,'Vichy'),(1054,'Victoria Beckham Beauty'),(260,'Victoria’s Secret'),(261,'Vidal Sassoon'),(262,'Viktor & Rolf'),(1055,'Vintner’s Daughter'),(1056,'Viori'),(1057,'Virtue'),(1058,'Vitruvi'),(1059,'Voir Haircare'),(1060,'Volition Beauty'),(1061,'W3ll People'),(1062,'Wabi-Sabi'),(1063,'Wander Beauty'),(1064,'We Are Paradoxx'),(1065,'We Love Eyes'),(1066,'Weleda'),(263,'Wella'),(1067,'WEN by Chaz Dean'),(1068,'Westman Atelier'),(1069,'Wet n Wild'),(1070,'Whamisa'),(1071,'Whish'),(1072,'Whole Foods (365)'),(264,'Windex'),(1073,'Winky Lux'),(1074,'Wishful'),(1075,'Wit & West Perfumes'),(1076,'WLDKAT'),(265,'Woolite'),(1077,'Wunderbrow'),(1078,'Yarok'),(1079,'Yes To'),(1080,'Yodi Beauty'),(1081,'Youngblood'),(1082,'Youth To The People'),(1083,'YUNI Beauty'),(266,'Yves Rocher'),(267,'Yves Saint Laurent'),(1084,'Zao Organic'),(268,'Zara Fragrance'),(1085,'Zitsticka'),(1086,'Zoeva'),(1087,'Zoya'),(1088,'Zuii Organic'),(1089,'ZuZu Luxe');
/*!40000 ALTER TABLE `brands` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `brands_cosmetics_types`
--

DROP TABLE IF EXISTS `brands_cosmetics_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `brands_cosmetics_types` (
  `brand_id` int NOT NULL,
  `cosmetics_type_id` int NOT NULL,
  PRIMARY KEY (`brand_id`,`cosmetics_type_id`),
  KEY `fk_brands_has_cosmetics_type_cosmetics_type1_idx` (`cosmetics_type_id`),
  KEY `fk_brands_has_cosmetics_type_brands1_idx` (`brand_id`),
  CONSTRAINT `fk_brands_has_cosmetics_type_brands1` FOREIGN KEY (`brand_id`) REFERENCES `brands` (`id`),
  CONSTRAINT `fk_brands_has_cosmetics_type_cosmetics_type1` FOREIGN KEY (`cosmetics_type_id`) REFERENCES `cosmetics_types` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `brands_cosmetics_types`
--

LOCK TABLES `brands_cosmetics_types` WRITE;
/*!40000 ALTER TABLE `brands_cosmetics_types` DISABLE KEYS */;
INSERT INTO `brands_cosmetics_types` VALUES (1,1),(4,1),(7,1),(8,1),(9,1),(14,1),(18,1),(23,1),(27,1),(35,1),(38,1),(39,1),(41,1),(45,1),(48,1),(52,1),(57,1),(59,1),(63,1),(69,1),(78,1),(81,1),(82,1),(90,1),(94,1),(95,1),(106,1),(107,1),(109,1),(114,1),(118,1),(121,1),(127,1),(139,1),(142,1),(143,1),(149,1),(150,1),(156,1),(157,1),(158,1),(160,1),(162,1),(163,1),(172,1),(176,1),(184,1),(189,1),(195,1),(196,1),(202,1),(209,1),(215,1),(226,1),(228,1),(229,1),(240,1),(248,1),(266,1),(267,1),(1,2),(3,2),(7,2),(8,2),(9,2),(11,2),(14,2),(16,2),(17,2),(18,2),(20,2),(23,2),(26,2),(29,2),(31,2),(32,2),(33,2),(34,2),(35,2),(36,2),(41,2),(49,2),(50,2),(51,2),(52,2),(53,2),(57,2),(58,2),(59,2),(60,2),(63,2),(69,2),(70,2),(72,2),(75,2),(78,2),(80,2),(81,2),(85,2),(86,2),(88,2),(90,2),(91,2),(92,2),(94,2),(95,2),(96,2),(97,2),(102,2),(103,2),(106,2),(107,2),(109,2),(114,2),(115,2),(118,2),(121,2),(123,2),(131,2),(133,2),(137,2),(138,2),(139,2),(141,2),(142,2),(143,2),(144,2),(145,2),(148,2),(149,2),(150,2),(153,2),(156,2),(158,2),(160,2),(175,2),(176,2),(181,2),(182,2),(183,2),(187,2),(189,2),(190,2),(195,2),(196,2),(199,2),(203,2),(204,2),(207,2),(209,2),(217,2),(218,2),(228,2),(229,2),(232,2),(234,2),(240,2),(248,2),(251,2),(254,2),(259,2),(266,2),(267,2),(2,3),(8,3),(9,3),(12,3),(14,3),(15,3),(18,3),(19,3),(21,3),(39,3),(40,3),(43,3),(44,3),(45,3),(48,3),(52,3),(54,3),(55,3),(69,3),(70,3),(73,3),(77,3),(79,3),(81,3),(82,3),(83,3),(87,3),(89,3),(90,3),(93,3),(94,3),(100,3),(103,3),(106,3),(107,3),(113,3),(114,3),(120,3),(124,3),(127,3),(128,3),(135,3),(141,3),(142,3),(146,3),(149,3),(151,3),(159,3),(160,3),(164,3),(165,3),(168,3),(179,3),(195,3),(198,3),(208,3),(211,3),(245,3),(248,3),(249,3),(253,3),(257,3),(258,3),(260,3),(262,3),(266,3),(267,3),(268,3),(5,4),(46,4),(99,4),(140,4),(185,4),(239,4),(6,5),(18,5),(57,5),(88,5),(137,5),(145,5),(181,5),(192,5),(240,5),(8,6),(11,6),(13,6),(16,6),(17,6),(18,6),(19,6),(31,6),(36,6),(44,6),(47,6),(49,6),(50,6),(51,6),(52,6),(53,6),(65,6),(68,6),(69,6),(70,6),(72,6),(75,6),(76,6),(78,6),(88,6),(91,6),(92,6),(94,6),(95,6),(102,6),(103,6),(121,6),(123,6),(125,6),(126,6),(131,6),(133,6),(137,6),(139,6),(141,6),(142,6),(143,6),(145,6),(147,6),(153,6),(154,6),(158,6),(160,6),(167,6),(174,6),(176,6),(181,6),(183,6),(189,6),(190,6),(192,6),(195,6),(196,6),(199,6),(200,6),(203,6),(204,6),(209,6),(210,6),(218,6),(236,6),(237,6),(245,6),(251,6),(254,6),(260,6),(266,6),(8,7),(9,7),(16,7),(24,7),(25,7),(31,7),(56,7),(61,7),(70,7),(95,7),(111,7),(116,7),(117,7),(121,7),(123,7),(129,7),(136,7),(137,7),(138,7),(139,7),(141,7),(142,7),(145,7),(161,7),(177,7),(180,7),(181,7),(188,7),(195,7),(200,7),(201,7),(203,7),(205,7),(209,7),(213,7),(221,7),(223,7),(224,7),(227,7),(242,7),(247,7),(251,7),(252,7),(261,7),(263,7),(266,7),(9,8),(65,8),(156,8),(157,8),(172,8),(193,8),(219,8),(230,8),(10,9),(66,9),(71,9),(152,9),(194,9),(209,9),(214,9),(222,9),(225,9),(13,10),(64,10),(67,10),(74,10),(98,10),(108,10),(112,10),(132,10),(134,10),(155,10),(169,10),(191,10),(197,10),(200,10),(206,10),(243,10),(264,10),(19,11),(76,11),(147,11),(167,11),(174,11),(192,11),(216,11),(236,11),(20,12),(29,12),(51,12),(57,12),(68,12),(75,12),(91,12),(123,12),(144,12),(148,12),(158,12),(228,12),(28,13),(105,13),(145,13),(171,13),(219,13),(220,13),(255,13),(256,13),(34,14),(37,15),(64,15),(84,15),(104,15),(112,15),(241,15),(246,15),(265,15),(56,16),(136,16),(142,16),(161,16),(173,16),(177,16),(178,16),(205,16),(213,16),(221,16),(224,16),(247,16),(261,16),(263,16),(57,17),(126,17),(199,17),(123,18);
/*!40000 ALTER TABLE `brands_cosmetics_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `certificates_attributes`
--

DROP TABLE IF EXISTS `certificates_attributes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `certificates_attributes` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name_UNIQUE` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=2973 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `certificates_attributes`
--

LOCK TABLES `certificates_attributes` WRITE;
/*!40000 ALTER TABLE `certificates_attributes` DISABLE KEYS */;
INSERT INTO `certificates_attributes` VALUES (3,'Leaping Bunny'),(1,'PETA'),(2,'Vegan');
/*!40000 ALTER TABLE `certificates_attributes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cfi`
--

DROP TABLE IF EXISTS `cfi`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `cfi` (
  `id` int NOT NULL AUTO_INCREMENT,
  `brand_id` int NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_cfi_brands1_idx` (`brand_id`),
  CONSTRAINT `fk_cfi_brands1` FOREIGN KEY (`brand_id`) REFERENCES `brands` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cfi`
--

LOCK TABLES `cfi` WRITE;
/*!40000 ALTER TABLE `cfi` DISABLE KEYS */;
/*!40000 ALTER TABLE `cfi` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cfk`
--

DROP TABLE IF EXISTS `cfk`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `cfk` (
  `id` int NOT NULL AUTO_INCREMENT,
  `brand_id` int NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `brand_id_UNIQUE` (`brand_id`),
  KEY `fk_cfk_brands1_idx` (`brand_id`),
  CONSTRAINT `fk_cfk_brands1` FOREIGN KEY (`brand_id`) REFERENCES `brands` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2464 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cfk`
--

LOCK TABLES `cfk` WRITE;
/*!40000 ALTER TABLE `cfk` DISABLE KEYS */;
INSERT INTO `cfk` VALUES (1,269),(2,270),(3,271),(4,272),(5,273),(6,274),(7,275),(8,276),(9,277),(10,278),(11,279),(12,280),(13,281),(14,282),(15,283),(16,284),(17,285),(18,286),(19,287),(20,288),(21,289),(22,290),(23,291),(24,292),(25,293),(26,294),(27,295),(28,296),(29,297),(30,298),(31,299),(32,300),(33,301),(34,302),(35,303),(36,304),(37,305),(38,306),(39,307),(40,308),(41,309),(42,310),(43,311),(44,312),(45,313),(46,314),(47,315),(48,316),(49,317),(50,318),(51,319),(52,320),(53,321),(54,322),(55,323),(56,324),(57,325),(58,326),(59,327),(60,328),(61,329),(62,330),(63,331),(64,332),(65,333),(66,334),(67,335),(68,336),(69,337),(70,338),(71,339),(72,340),(73,341),(74,342),(75,343),(76,344),(77,345),(78,346),(79,347),(80,348),(81,349),(82,350),(83,351),(84,352),(85,353),(86,354),(87,355),(88,356),(89,357),(90,358),(91,359),(92,360),(93,361),(94,362),(95,363),(96,364),(97,365),(98,366),(99,367),(100,368),(101,369),(102,370),(103,371),(104,372),(105,373),(106,374),(107,375),(108,376),(109,377),(110,378),(111,379),(112,380),(113,381),(114,382),(115,383),(116,384),(117,385),(118,386),(119,387),(120,388),(121,389),(122,390),(123,391),(124,392),(125,393),(126,394),(127,395),(128,396),(129,397),(130,398),(131,399),(132,400),(133,401),(134,402),(135,403),(136,404),(137,405),(138,406),(139,407),(140,408),(141,409),(142,410),(143,411),(144,412),(145,413),(146,414),(147,415),(148,416),(149,417),(150,418),(151,419),(152,420),(153,421),(154,422),(155,423),(156,424),(157,425),(158,426),(159,427),(160,428),(161,429),(162,430),(163,431),(164,432),(165,433),(166,434),(167,435),(168,436),(169,437),(170,438),(171,439),(172,440),(173,441),(174,442),(175,443),(176,444),(177,445),(178,446),(179,447),(180,448),(181,449),(182,450),(183,451),(184,452),(185,453),(186,454),(187,455),(188,456),(189,457),(190,458),(191,459),(192,460),(193,461),(194,462),(195,463),(196,464),(197,465),(198,466),(199,467),(200,468),(201,469),(202,470),(203,471),(204,472),(205,473),(206,474),(207,475),(208,476),(209,477),(210,478),(211,479),(212,480),(213,481),(214,482),(215,483),(216,484),(217,485),(218,486),(219,487),(220,488),(221,489),(222,490),(223,491),(224,492),(225,493),(226,494),(227,495),(228,496),(229,497),(230,498),(231,499),(232,500),(233,501),(234,502),(235,503),(236,504),(237,505),(238,506),(239,507),(240,508),(241,509),(242,510),(243,511),(244,512),(245,513),(246,514),(247,515),(248,516),(249,517),(250,518),(251,519),(252,520),(253,521),(254,522),(255,523),(256,524),(257,525),(258,526),(259,527),(260,528),(261,529),(262,530),(263,531),(264,532),(265,533),(266,534),(267,535),(268,536),(269,537),(270,538),(271,539),(272,540),(273,541),(274,542),(275,543),(276,544),(277,545),(278,546),(279,547),(280,548),(281,549),(282,550),(283,551),(284,552),(285,553),(286,554),(287,555),(288,556),(289,557),(290,558),(291,559),(292,560),(293,561),(294,562),(295,563),(296,564),(297,565),(298,566),(299,567),(300,568),(301,569),(302,570),(303,571),(304,572),(305,573),(306,574),(307,575),(308,576),(309,577),(310,578),(311,579),(312,580),(313,581),(314,582),(315,583),(316,584),(317,585),(318,586),(319,587),(320,588),(321,589),(322,590),(323,591),(324,592),(325,593),(326,594),(327,595),(328,596),(329,597),(330,598),(331,599),(332,600),(333,601),(334,602),(335,603),(336,604),(337,605),(338,606),(339,607),(340,608),(341,609),(342,610),(343,611),(344,612),(345,613),(346,614),(347,615),(348,616),(349,617),(350,618),(351,619),(352,620),(353,621),(354,622),(355,623),(356,624),(357,625),(358,626),(359,627),(360,628),(361,629),(362,630),(363,631),(364,632),(365,633),(366,634),(367,635),(368,636),(369,637),(370,638),(371,639),(372,640),(373,641),(374,642),(375,643),(376,644),(377,645),(378,646),(379,647),(380,648),(381,649),(382,650),(383,651),(384,652),(385,653),(386,654),(387,655),(388,656),(389,657),(390,658),(391,659),(392,660),(393,661),(394,662),(395,663),(396,664),(397,665),(398,666),(399,667),(400,668),(401,669),(402,670),(403,671),(404,672),(405,673),(406,674),(407,675),(408,676),(409,677),(410,678),(411,679),(412,680),(413,681),(414,682),(415,683),(416,684),(417,685),(418,686),(419,687),(420,688),(421,689),(422,690),(423,691),(424,692),(425,693),(426,694),(427,695),(428,696),(429,697),(430,698),(431,699),(432,700),(433,701),(434,702),(435,703),(436,704),(437,705),(438,706),(439,707),(440,708),(441,709),(442,710),(443,711),(444,712),(445,713),(446,714),(447,715),(448,716),(449,717),(450,718),(451,719),(452,720),(453,721),(454,722),(455,723),(456,724),(457,725),(458,726),(459,727),(460,728),(461,729),(462,730),(463,731),(464,732),(465,733),(466,734),(467,735),(468,736),(469,737),(470,738),(471,739),(472,740),(473,741),(474,742),(475,743),(476,744),(477,745),(478,746),(479,747),(480,748),(481,749),(482,750),(483,751),(484,752),(485,753),(486,754),(487,755),(488,756),(489,757),(490,758),(491,759),(492,760),(493,761),(494,762),(495,763),(496,764),(497,765),(498,766),(499,767),(500,768),(501,769),(502,770),(503,771),(504,772),(505,773),(506,774),(507,775),(508,776),(509,777),(510,778),(511,779),(512,780),(513,781),(514,782),(515,783),(516,784),(517,785),(518,786),(519,787),(520,788),(521,789),(522,790),(523,791),(524,792),(525,793),(526,794),(527,795),(528,796),(529,797),(530,798),(531,799),(532,800),(533,801),(534,802),(535,803),(536,804),(537,805),(538,806),(539,807),(540,808),(541,809),(542,810),(543,811),(544,812),(545,813),(546,814),(547,815),(548,816),(549,817),(550,818),(551,819),(552,820),(553,821),(554,822),(555,823),(556,824),(557,825),(558,826),(559,827),(560,828),(561,829),(562,830),(563,831),(564,832),(565,833),(566,834),(567,835),(568,836),(569,837),(570,838),(571,839),(572,840),(573,841),(574,842),(575,843),(576,844),(577,845),(578,846),(579,847),(580,848),(581,849),(582,850),(583,851),(584,852),(585,853),(586,854),(587,855),(588,856),(589,857),(590,858),(591,859),(592,860),(593,861),(594,862),(595,863),(596,864),(597,865),(598,866),(599,867),(600,868),(601,869),(602,870),(603,871),(604,872),(605,873),(606,874),(607,875),(608,876),(609,877),(610,878),(611,879),(612,880),(613,881),(614,882),(615,883),(616,884),(617,885),(618,886),(619,887),(620,888),(621,889),(622,890),(623,891),(624,892),(625,893),(626,894),(627,895),(628,896),(629,897),(630,898),(631,899),(632,900),(633,901),(634,902),(635,903),(636,904),(637,905),(638,906),(639,907),(640,908),(641,909),(642,910),(643,911),(644,912),(645,913),(646,914),(647,915),(648,916),(649,917),(650,918),(651,919),(652,920),(653,921),(654,922),(655,923),(656,924),(657,925),(658,926),(659,927),(660,928),(661,929),(662,930),(663,931),(664,932),(665,933),(666,934),(667,935),(668,936),(669,937),(670,938),(671,939),(672,940),(673,941),(674,942),(675,943),(676,944),(677,945),(678,946),(679,947),(680,948),(681,949),(682,950),(683,951),(684,952),(685,953),(686,954),(687,955),(688,956),(689,957),(690,958),(691,959),(692,960),(693,961),(694,962),(695,963),(696,964),(697,965),(698,966),(699,967),(700,968),(701,969),(702,970),(703,971),(704,972),(705,973),(706,974),(707,975),(708,976),(709,977),(710,978),(711,979),(712,980),(713,981),(714,982),(715,983),(716,984),(717,985),(718,986),(719,987),(720,988),(721,989),(722,990),(723,991),(724,992),(725,993),(726,994),(727,995),(728,996),(729,997),(730,998),(731,999),(732,1000),(733,1001),(734,1002),(735,1003),(736,1004),(737,1005),(738,1006),(739,1007),(740,1008),(741,1009),(742,1010),(743,1011),(744,1012),(745,1013),(746,1014),(747,1015),(748,1016),(749,1017),(750,1018),(751,1019),(752,1020),(753,1021),(754,1022),(755,1023),(756,1024),(757,1025),(758,1026),(759,1027),(760,1028),(761,1029),(762,1030),(763,1031),(764,1032),(765,1033),(766,1034),(767,1035),(768,1036),(769,1037),(770,1038),(771,1039),(772,1040),(773,1041),(774,1042),(775,1043),(776,1044),(777,1045),(778,1046),(779,1047),(780,1048),(781,1049),(782,1050),(783,1051),(784,1052),(785,1053),(786,1054),(787,1055),(788,1056),(789,1057),(790,1058),(791,1059),(792,1060),(793,1061),(794,1062),(795,1063),(796,1064),(797,1065),(798,1066),(799,1067),(800,1068),(801,1069),(802,1070),(803,1071),(804,1072),(805,1073),(806,1074),(807,1075),(808,1076),(809,1077),(810,1078),(811,1079),(812,1080),(813,1081),(814,1082),(815,1083),(816,1084),(817,1085),(818,1086),(819,1087),(820,1088),(821,1089);
/*!40000 ALTER TABLE `cfk` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cfk_certificates_attributes`
--

DROP TABLE IF EXISTS `cfk_certificates_attributes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `cfk_certificates_attributes` (
  `cfk_id` int NOT NULL,
  `certificates_attributes_id` int NOT NULL,
  PRIMARY KEY (`cfk_id`,`certificates_attributes_id`),
  KEY `fk_cfk_has_certificates_attributes_certificates_attributes1_idx` (`certificates_attributes_id`),
  KEY `fk_cfk_has_certificates_attributes_cfk1_idx` (`cfk_id`),
  CONSTRAINT `fk_cfk_has_certificates_attributes_certificates_attributes1` FOREIGN KEY (`certificates_attributes_id`) REFERENCES `certificates_attributes` (`id`),
  CONSTRAINT `fk_cfk_has_certificates_attributes_cfk1` FOREIGN KEY (`cfk_id`) REFERENCES `cfk` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cfk_certificates_attributes`
--

LOCK TABLES `cfk_certificates_attributes` WRITE;
/*!40000 ALTER TABLE `cfk_certificates_attributes` DISABLE KEYS */;
INSERT INTO `cfk_certificates_attributes` VALUES (1,1),(4,1),(6,1),(8,1),(9,1),(10,1),(12,1),(13,1),(14,1),(15,1),(18,1),(20,1),(27,1),(28,1),(29,1),(32,1),(36,1),(37,1),(38,1),(41,1),(43,1),(44,1),(45,1),(46,1),(47,1),(48,1),(50,1),(51,1),(55,1),(56,1),(57,1),(58,1),(59,1),(60,1),(61,1),(63,1),(68,1),(69,1),(79,1),(80,1),(84,1),(85,1),(91,1),(97,1),(98,1),(99,1),(100,1),(104,1),(105,1),(113,1),(118,1),(119,1),(122,1),(123,1),(125,1),(127,1),(128,1),(130,1),(136,1),(137,1),(141,1),(142,1),(145,1),(149,1),(153,1),(154,1),(158,1),(159,1),(160,1),(161,1),(165,1),(166,1),(167,1),(169,1),(170,1),(171,1),(173,1),(176,1),(179,1),(182,1),(192,1),(194,1),(196,1),(198,1),(199,1),(200,1),(201,1),(202,1),(203,1),(204,1),(205,1),(206,1),(207,1),(212,1),(213,1),(214,1),(216,1),(217,1),(219,1),(220,1),(222,1),(223,1),(224,1),(225,1),(226,1),(229,1),(231,1),(237,1),(238,1),(241,1),(242,1),(248,1),(249,1),(250,1),(252,1),(253,1),(254,1),(256,1),(258,1),(259,1),(260,1),(262,1),(268,1),(274,1),(275,1),(285,1),(286,1),(287,1),(293,1),(294,1),(296,1),(304,1),(306,1),(307,1),(308,1),(311,1),(314,1),(320,1),(324,1),(325,1),(326,1),(330,1),(331,1),(334,1),(335,1),(338,1),(339,1),(344,1),(346,1),(347,1),(350,1),(351,1),(352,1),(355,1),(358,1),(360,1),(361,1),(364,1),(367,1),(370,1),(371,1),(375,1),(377,1),(380,1),(381,1),(382,1),(384,1),(385,1),(387,1),(390,1),(391,1),(392,1),(394,1),(396,1),(398,1),(405,1),(413,1),(415,1),(416,1),(421,1),(426,1),(427,1),(432,1),(433,1),(435,1),(439,1),(441,1),(444,1),(447,1),(450,1),(452,1),(455,1),(457,1),(463,1),(464,1),(469,1),(473,1),(475,1),(476,1),(477,1),(478,1),(485,1),(486,1),(487,1),(488,1),(490,1),(491,1),(498,1),(503,1),(505,1),(506,1),(507,1),(513,1),(515,1),(517,1),(518,1),(522,1),(523,1),(524,1),(532,1),(533,1),(542,1),(543,1),(545,1),(546,1),(547,1),(548,1),(549,1),(552,1),(555,1),(561,1),(567,1),(569,1),(573,1),(578,1),(579,1),(580,1),(585,1),(589,1),(591,1),(596,1),(601,1),(605,1),(606,1),(608,1),(610,1),(615,1),(617,1),(623,1),(625,1),(632,1),(633,1),(638,1),(641,1),(644,1),(649,1),(655,1),(656,1),(657,1),(659,1),(662,1),(664,1),(665,1),(666,1),(667,1),(668,1),(669,1),(670,1),(674,1),(678,1),(680,1),(683,1),(687,1),(688,1),(696,1),(700,1),(701,1),(702,1),(705,1),(707,1),(709,1),(717,1),(718,1),(719,1),(720,1),(721,1),(725,1),(727,1),(729,1),(732,1),(734,1),(737,1),(740,1),(743,1),(745,1),(747,1),(748,1),(749,1),(750,1),(751,1),(752,1),(753,1),(754,1),(756,1),(759,1),(760,1),(761,1),(762,1),(763,1),(764,1),(765,1),(771,1),(773,1),(779,1),(781,1),(782,1),(792,1),(793,1),(794,1),(796,1),(801,1),(805,1),(810,1),(811,1),(813,1),(814,1),(820,1),(821,1),(2,2),(3,2),(6,2),(7,2),(8,2),(9,2),(10,2),(12,2),(13,2),(16,2),(19,2),(21,2),(24,2),(26,2),(31,2),(36,2),(39,2),(42,2),(43,2),(44,2),(45,2),(46,2),(47,2),(56,2),(57,2),(58,2),(59,2),(63,2),(64,2),(67,2),(69,2),(70,2),(72,2),(73,2),(75,2),(87,2),(88,2),(90,2),(92,2),(93,2),(95,2),(96,2),(100,2),(102,2),(105,2),(107,2),(109,2),(110,2),(111,2),(112,2),(114,2),(116,2),(120,2),(124,2),(126,2),(127,2),(130,2),(134,2),(141,2),(142,2),(143,2),(147,2),(155,2),(156,2),(160,2),(161,2),(162,2),(164,2),(165,2),(168,2),(170,2),(174,2),(176,2),(178,2),(179,2),(182,2),(184,2),(186,2),(195,2),(197,2),(198,2),(199,2),(200,2),(204,2),(207,2),(208,2),(210,2),(213,2),(215,2),(216,2),(218,2),(219,2),(225,2),(226,2),(230,2),(231,2),(235,2),(239,2),(241,2),(242,2),(243,2),(245,2),(247,2),(248,2),(250,2),(252,2),(254,2),(256,2),(264,2),(265,2),(268,2),(269,2),(270,2),(272,2),(274,2),(276,2),(277,2),(279,2),(281,2),(283,2),(284,2),(286,2),(291,2),(293,2),(294,2),(298,2),(302,2),(303,2),(304,2),(305,2),(306,2),(315,2),(320,2),(321,2),(323,2),(328,2),(330,2),(332,2),(333,2),(335,2),(337,2),(341,2),(343,2),(345,2),(348,2),(349,2),(352,2),(353,2),(358,2),(369,2),(371,2),(373,2),(375,2),(380,2),(386,2),(388,2),(392,2),(399,2),(401,2),(402,2),(403,2),(408,2),(411,2),(414,2),(415,2),(419,2),(426,2),(429,2),(431,2),(434,2),(435,2),(437,2),(438,2),(440,2),(446,2),(450,2),(452,2),(453,2),(456,2),(457,2),(458,2),(462,2),(465,2),(467,2),(469,2),(471,2),(476,2),(477,2),(478,2),(490,2),(496,2),(497,2),(499,2),(500,2),(502,2),(503,2),(504,2),(506,2),(508,2),(510,2),(517,2),(519,2),(520,2),(525,2),(532,2),(533,2),(540,2),(545,2),(547,2),(550,2),(551,2),(553,2),(556,2),(557,2),(560,2),(561,2),(562,2),(567,2),(569,2),(570,2),(574,2),(581,2),(584,2),(587,2),(590,2),(591,2),(592,2),(594,2),(595,2),(596,2),(600,2),(602,2),(605,2),(606,2),(609,2),(610,2),(611,2),(612,2),(613,2),(614,2),(616,2),(617,2),(618,2),(620,2),(621,2),(622,2),(623,2),(627,2),(629,2),(630,2),(635,2),(636,2),(638,2),(645,2),(648,2),(650,2),(654,2),(656,2),(657,2),(659,2),(660,2),(662,2),(663,2),(665,2),(667,2),(668,2),(677,2),(681,2),(683,2),(685,2),(687,2),(690,2),(691,2),(693,2),(694,2),(698,2),(703,2),(704,2),(706,2),(709,2),(710,2),(713,2),(714,2),(716,2),(718,2),(722,2),(723,2),(724,2),(725,2),(733,2),(734,2),(735,2),(737,2),(740,2),(744,2),(750,2),(757,2),(758,2),(759,2),(760,2),(762,2),(764,2),(766,2),(767,2),(770,2),(773,2),(774,2),(776,2),(777,2),(779,2),(780,2),(784,2),(785,2),(788,2),(790,2),(794,2),(796,2),(797,2),(807,2),(808,2),(810,2),(812,2),(814,2),(815,2),(816,2),(819,2),(2,3),(3,3),(6,3),(9,3),(11,3),(12,3),(14,3),(18,3),(21,3),(22,3),(23,3),(24,3),(26,3),(27,3),(29,3),(34,3),(35,3),(42,3),(43,3),(44,3),(50,3),(54,3),(55,3),(56,3),(57,3),(61,3),(62,3),(64,3),(65,3),(66,3),(67,3),(70,3),(72,3),(73,3),(75,3),(78,3),(80,3),(81,3),(82,3),(87,3),(88,3),(89,3),(92,3),(93,3),(94,3),(95,3),(96,3),(100,3),(102,3),(105,3),(107,3),(108,3),(109,3),(111,3),(112,3),(114,3),(115,3),(116,3),(117,3),(119,3),(124,3),(126,3),(129,3),(132,3),(133,3),(138,3),(140,3),(143,3),(146,3),(150,3),(154,3),(156,3),(157,3),(159,3),(162,3),(163,3),(165,3),(170,3),(177,3),(179,3),(181,3),(182,3),(184,3),(189,3),(190,3),(196,3),(198,3),(205,3),(207,3),(209,3),(210,3),(211,3),(213,3),(214,3),(222,3),(227,3),(228,3),(230,3),(231,3),(233,3),(234,3),(235,3),(236,3),(238,3),(239,3),(241,3),(243,3),(244,3),(247,3),(248,3),(250,3),(252,3),(259,3),(260,3),(264,3),(266,3),(267,3),(269,3),(275,3),(276,3),(277,3),(278,3),(281,3),(286,3),(288,3),(291,3),(297,3),(300,3),(302,3),(305,3),(307,3),(308,3),(310,3),(311,3),(316,3),(317,3),(318,3),(321,3),(327,3),(329,3),(330,3),(332,3),(333,3),(337,3),(338,3),(341,3),(344,3),(347,3),(352,3),(356,3),(358,3),(361,3),(363,3),(365,3),(366,3),(368,3),(369,3),(371,3),(372,3),(374,3),(378,3),(382,3),(384,3),(387,3),(389,3),(392,3),(394,3),(397,3),(398,3),(401,3),(402,3),(404,3),(408,3),(411,3),(415,3),(418,3),(419,3),(420,3),(421,3),(425,3),(428,3),(430,3),(431,3),(444,3),(452,3),(456,3),(457,3),(460,3),(461,3),(462,3),(464,3),(465,3),(466,3),(471,3),(477,3),(478,3),(480,3),(482,3),(484,3),(490,3),(493,3),(494,3),(497,3),(501,3),(503,3),(504,3),(506,3),(507,3),(508,3),(509,3),(510,3),(511,3),(512,3),(513,3),(515,3),(516,3),(517,3),(522,3),(531,3),(533,3),(534,3),(540,3),(541,3),(549,3),(550,3),(554,3),(557,3),(560,3),(561,3),(566,3),(568,3),(570,3),(571,3),(572,3),(575,3),(579,3),(581,3),(582,3),(584,3),(587,3),(588,3),(590,3),(592,3),(595,3),(596,3),(598,3),(599,3),(600,3),(603,3),(607,3),(608,3),(613,3),(614,3),(615,3),(628,3),(630,3),(631,3),(633,3),(634,3),(636,3),(637,3),(645,3),(649,3),(650,3),(652,3),(653,3),(659,3),(664,3),(667,3),(676,3),(681,3),(682,3),(683,3),(685,3),(687,3),(691,3),(693,3),(696,3),(701,3),(702,3),(703,3),(704,3),(705,3),(707,3),(709,3),(710,3),(711,3),(712,3),(713,3),(716,3),(720,3),(725,3),(727,3),(730,3),(731,3),(732,3),(733,3),(736,3),(739,3),(741,3),(742,3),(744,3),(745,3),(746,3),(758,3),(759,3),(760,3),(767,3),(768,3),(772,3),(773,3),(774,3),(775,3),(776,3),(777,3),(778,3),(780,3),(782,3),(784,3),(787,3),(788,3),(789,3),(791,3),(793,3),(796,3),(797,3),(799,3),(803,3),(807,3),(808,3),(809,3),(810,3),(811,3),(814,3),(815,3),(821,3);
/*!40000 ALTER TABLE `cfk_certificates_attributes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cfk_certified_not`
--

DROP TABLE IF EXISTS `cfk_certified_not`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `cfk_certified_not` (
  `id` int NOT NULL AUTO_INCREMENT,
  `brand_id` int NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `brand_id_UNIQUE` (`brand_id`),
  KEY `fk_cfk_certified_not_brands1_idx` (`brand_id`),
  CONSTRAINT `fk_cfk_certified_not_brands1` FOREIGN KEY (`brand_id`) REFERENCES `brands` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=537 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cfk_certified_not`
--

LOCK TABLES `cfk_certified_not` WRITE;
/*!40000 ALTER TABLE `cfk_certified_not` DISABLE KEYS */;
INSERT INTO `cfk_certified_not` VALUES (1,1),(2,2),(3,3),(4,4),(5,5),(6,6),(7,7),(8,8),(9,9),(10,10),(11,11),(12,12),(13,13),(14,14),(15,15),(16,16),(17,17),(18,18),(19,19),(20,20),(21,21),(22,22),(23,23),(24,24),(25,25),(26,26),(27,27),(28,28),(29,29),(30,30),(31,31),(32,32),(33,33),(34,34),(35,35),(36,36),(37,37),(38,38),(39,39),(40,40),(41,41),(42,42),(43,43),(44,44),(45,45),(46,46),(47,47),(48,48),(49,49),(50,50),(51,51),(52,52),(53,53),(54,54),(55,55),(56,56),(57,57),(58,58),(59,59),(60,60),(61,61),(62,62),(63,63),(64,64),(65,65),(66,66),(67,67),(68,68),(69,69),(70,70),(71,71),(72,72),(73,73),(74,74),(75,75),(76,76),(77,77),(78,78),(79,79),(80,80),(81,81),(82,82),(83,83),(84,84),(85,85),(86,86),(87,87),(88,88),(89,89),(90,90),(91,91),(92,92),(93,93),(94,94),(95,95),(96,96),(97,97),(98,98),(99,99),(100,100),(101,101),(102,102),(103,103),(104,104),(105,105),(106,106),(107,107),(108,108),(109,109),(110,110),(111,111),(112,112),(113,113),(114,114),(115,115),(116,116),(117,117),(118,118),(119,119),(120,120),(121,121),(122,122),(123,123),(124,124),(125,125),(126,126),(127,127),(128,128),(129,129),(130,130),(131,131),(132,132),(133,133),(134,134),(135,135),(136,136),(137,137),(138,138),(139,139),(140,140),(141,141),(142,142),(143,143),(144,144),(145,145),(146,146),(147,147),(148,148),(149,149),(150,150),(151,151),(152,152),(153,153),(154,154),(155,155),(156,156),(157,157),(158,158),(159,159),(160,160),(161,161),(162,162),(163,163),(164,164),(165,165),(166,166),(167,167),(168,168),(169,169),(170,170),(171,171),(172,172),(173,173),(174,174),(175,175),(176,176),(177,177),(178,178),(179,179),(180,180),(181,181),(182,182),(183,183),(184,184),(185,185),(186,186),(187,187),(188,188),(189,189),(190,190),(191,191),(192,192),(193,193),(194,194),(195,195),(196,196),(197,197),(198,198),(199,199),(200,200),(201,201),(202,202),(203,203),(204,204),(205,205),(206,206),(207,207),(208,208),(209,209),(210,210),(211,211),(212,212),(213,213),(214,214),(215,215),(216,216),(217,217),(218,218),(219,219),(220,220),(221,221),(222,222),(223,223),(224,224),(225,225),(226,226),(227,227),(228,228),(229,229),(230,230),(231,231),(232,232),(233,233),(234,234),(235,235),(236,236),(237,237),(238,238),(239,239),(240,240),(241,241),(242,242),(243,243),(244,244),(245,245),(246,246),(247,247),(248,248),(249,249),(250,250),(251,251),(252,252),(253,253),(254,254),(255,255),(256,256),(257,257),(258,258),(259,259),(260,260),(261,261),(262,262),(263,263),(264,264),(265,265),(266,266),(267,267),(268,268);
/*!40000 ALTER TABLE `cfk_certified_not` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cosmetics_types`
--

DROP TABLE IF EXISTS `cosmetics_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `cosmetics_types` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name_UNIQUE` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=907 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cosmetics_types`
--

LOCK TABLES `cosmetics_types` WRITE;
/*!40000 ALTER TABLE `cosmetics_types` DISABLE KEYS */;
INSERT INTO `cosmetics_types` VALUES (18,'Baby'),(6,'Body Care'),(10,'Cleaning'),(11,'Deodorant'),(17,'Fake Tan'),(5,'For Men'),(3,'Fragrance'),(7,'Hair Care'),(16,'Hair Dye'),(13,'Hair Removal'),(15,'Laundry'),(14,'Lip Balm'),(1,'Makeup'),(8,'Nail Polish'),(9,'Oral Care'),(4,'Period Products'),(2,'Skincare'),(12,'Sunscreen');
/*!40000 ALTER TABLE `cosmetics_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lb`
--

DROP TABLE IF EXISTS `lb`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `lb` (
  `id` int NOT NULL AUTO_INCREMENT,
  `brand_id` int NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_lb_brands1_idx` (`brand_id`),
  CONSTRAINT `fk_lb_brands1` FOREIGN KEY (`brand_id`) REFERENCES `brands` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lb`
--

LOCK TABLES `lb` WRITE;
/*!40000 ALTER TABLE `lb` DISABLE KEYS */;
/*!40000 ALTER TABLE `lb` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `peta`
--

DROP TABLE IF EXISTS `peta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `peta` (
  `id` int NOT NULL AUTO_INCREMENT,
  `brand_id` int NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_peta_brands1_idx` (`brand_id`),
  CONSTRAINT `fk_peta_brands1` FOREIGN KEY (`brand_id`) REFERENCES `brands` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `peta`
--

LOCK TABLES `peta` WRITE;
/*!40000 ALTER TABLE `peta` DISABLE KEYS */;
/*!40000 ALTER TABLE `peta` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2023-04-20 15:58:04
