import mysql.connector


def create_connection(credentials):
    return mysql.connector.connect(
        host = credentials[0],
        user = credentials[1],
        password = credentials[2],
        database = credentials[3],
    )

def bulk_insert_query(connection, sql_query, sql_params=()):
    with connection.cursor() as curr:
        # curr = connection.cursor()
        curr.executemany(sql_query, sql_params)
        connection.commit()
        # curr.close()

def read_query(connection, sql_query, sql_params=()):
    with connection.cursor() as curr:
        curr.execute(sql_query, sql_params)
        return curr.fetchall()

