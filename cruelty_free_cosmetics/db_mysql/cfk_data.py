from db_mysql.db_mysql import bulk_insert_query, read_query


BRANDS_QUERY = """insert into brands(name) values(%s) as new_data on duplicate key update name = new_data.name"""
SELECT_IN_BRANDS_QUERY = '''select * from brands where `name` in'''
COSMETICS_TYPES_QUERY = """insert into cosmetics_types(name) values(%s) as new_data on duplicate key update name = new_data.name"""
SELECT_IN_COSMETICS_TYPES_QUERY = '''select * from cosmetics_types where `name` in'''
BRANDS_COSMETICS_TYPES_QUERY2 = """insert into brands_cosmetics_types(brand_id, cosmetics_type_id) values(%s,%s) as new_data on duplicate key update brand_id = new_data.brand_id, cosmetics_type_id = new_data.cosmetics_type_id"""
BRANDS_COSMETICS_TYPES_QUERY = '''insert into brands_cosmetics_types (brand_id, cosmetics_type_id)
                                    select %s, c.id
                                    from cosmetics_types as c
                                    where c.`name` = %s
                                    on duplicate key update brand_id = brand_id, cosmetics_type_id = c.id'''
CERTIFICATES_ATTRIBUTES_QUERY = '''insert into certificates_attributes(name) values(%s) as new_data on duplicate key update name = new_data.name'''
SELECT_IN_CERTIFICATES_ATTRIBUTES_QUERY = '''select * from certificates_attributes where `name` in'''
CFK_CERTIFICATES_ATTRIBUTES_QUERY2 = '''insert into cfk_certificates_attributes(cfk_id, certificates_attributes_id) values(%s,%s) as new_data on duplicate key update cfk_id = new_data.cfk_id, certificates_attributes_id = new_data.certificates_attributes_id'''

CFK_CERTIFICATES_ATTRIBUTES_QUERY = '''insert into cfk_certificates_attributes (cfk_id, certificates_attributes_id)
                                        select c.id, ca.id
                                        from cfk as c, certificates_attributes as ca, brands as b
                                        where b.id = %s
                                        and c.brand_id = b.id
                                        and ca.`name` = %s
                                        on duplicate key update cfk_id = c.id, certificates_attributes_id = ca.id'''
CFK_QUERY = """insert into cfk(brand_id) values(%s) as new_data on duplicate key update brand_id = new_data.brand_id"""
CFK_CERTIFIED_NOT_QUERY = '''insert into cfk_certified_not(brand_id) values(%s) as new_data on duplicate key update brand_id = new_data.brand_id'''
SELECT_CFK_CERTIFIED_NOT_QUERY = '''select * from cfk_certified_not where `name` in'''
SELECT_IN_CFK_BRANDS_QUERY = '''select b.`name`, a.id from cfk as a join brands as b on b.id = a.brand_id where b.id in'''


def cfk_update_db(connection, collection):
    pass


def cfk_store_db(connection, collection):
    _add_many_brands(connection, collection)
    brands = _get_many_brands(connection, collection)

    _add_brands_ids_to_collection(collection, brands)
    _add_many_cfk(connection, collection)
    _add_many_cfk_not(connection, collection)
    _add_many_cosm_types_cfk(connection, collection)
    _add_many_cert_attr_cfk(connection, collection)

def _get_many_brands(connection, collection):
    brands = _get_many(connection, collection.get_spider_brands(), SELECT_IN_BRANDS_QUERY)
    return brands

def _add_brands_ids_to_collection(collection, brands):
    collection.add_brands_ids(brands)

def _add_many_cert_attr_cfk(connection, collection):
    temp_collection = collection.get_unique_certificates_attributes()
    if temp_collection:
        # Insert to certificates_attributes
        bulk_insert_query(connection, CERTIFICATES_ATTRIBUTES_QUERY, collection.get_certificates_attributes_as_tuples())
        # Insert to cfk_certificates_attributes
        bulk_insert_query(connection, CFK_CERTIFICATES_ATTRIBUTES_QUERY, collection.get_brands_certificates_attributes())

def _add_many_cosm_types_cfk(connection, collection):
    temp_collection = collection.get_unique_cosmetics_types()
    if temp_collection:
        # Insert to cosmetics_types
        bulk_insert_query(connection, COSMETICS_TYPES_QUERY, collection.get_cosmetics_types_as_tuples())
        # Insert to brands_cosmetics_types
        bulk_insert_query(connection, BRANDS_COSMETICS_TYPES_QUERY, collection.get_brands_with_types())
    
def _add_many_cfk_not(connection, collection):
    temp_collection = collection.get_not_certified_items()
    if temp_collection:
        bulk_insert_query(connection, CFK_CERTIFIED_NOT_QUERY, temp_collection)

def _add_many_cfk(connection, collection):
    temp_collection = collection.get_certified_items()
    if temp_collection:
        bulk_insert_query(connection, CFK_QUERY, temp_collection)

def _get_many(connection, temp_collection: tuple, statement):
    sql_query = _compose_select_in_query(statement, len(temp_collection))
    result = read_query(connection, sql_query, temp_collection)
    return result

def _add_many_brands(connection, collection):
    bulk_insert_query(connection, BRANDS_QUERY, collection.get_spider_brands_as_tuples())

def _compose_select_in_query(select_query, arguments_number):
    sql_query_part = ''','''.join(['''%s''' for _ in range(arguments_number)])
    return f'''{select_query} ({sql_query_part})'''
