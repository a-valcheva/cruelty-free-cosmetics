import os
import time

DIR_PATH = os.path.dirname(__file__)
BACKUP_DIR = os.path.join(DIR_PATH, 'db_backup_files/')

# To get credentials from pipeline
# credentials = get_credentials('common/mysql_credentials.txt')

def backup_db(host, user, password, database):
    date_format = '%Y-%m-%d_%H-%M-%S'

    current_time = time.strftime(date_format)
    backup_file = f'{database}-{current_time}.sql'
    backup_file_path = os.path.join(BACKUP_DIR, backup_file)

    mysqldump_cmd = f'mysqldump -h {host} -u {user} -p{password} {database} > {backup_file_path}'
    os.system(mysqldump_cmd)

    # gzip_cmd = f'gzip {backup_file_path}'
    # os.system(gzip_cmd)

    # Delete files older than 7 days
    #find_cmd = f'find {BACKUP_DIR} -type f -name "*.gz" -mtime +7 -delete'
    #os.system(find_cmd)
