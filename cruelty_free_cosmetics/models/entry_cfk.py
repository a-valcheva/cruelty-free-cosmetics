class EntryCFK:
    def __init__(self, name: str, is_cf: bool, certificates_attributes, cosmetics_types) -> None:
        self.name = name
        self.is_cf = is_cf
        self.certificates_attributes = certificates_attributes
        self.cosmetics_types = cosmetics_types
        self.brand_id = None

    @property
    def name(self):
        return self._name
    
    @name.setter
    def name(self, value):
        if not value:
            raise ValueError()
        self._name = value

    def get_brand_id_types(self):
        if self.brand_id and self.cosmetics_types:
            return [(self.brand_id, val) for val in self.cosmetics_types]

    def get_brand_id_certificats(self):
        if self.brand_id and self.certificates_attributes:
            return [(self.brand_id, val) for val in self.certificates_attributes]
