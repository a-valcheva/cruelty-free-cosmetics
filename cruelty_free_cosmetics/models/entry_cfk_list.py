# from cruelty_free_cosmetics.models.entry_cfk import EntryCFK
from models.entry_cfk import EntryCFK


class EntryCFKList:
    def __init__(self, collection: list[EntryCFK] = None) -> None:
        if collection is None:
            self.collection = []
        else:
            self.collection = collection

    # @property
    # def collection(self):
    #     return self.collection
    
    @property
    def lendth(self):
        return len(self.collection)

    def add_item(self, item: EntryCFK):
        if not isinstance(item, EntryCFK):
            raise TypeError()
        self.collection.append(item)

    def get_spider_brands(self):
        return tuple(brand.name for brand in self.collection)

    def get_spider_brands_as_tuples(self):
        return tuple((brand.name,) for brand in self.collection)
    
    def get_cosmetics_types_as_tuples(self):
        return tuple((val,) for vals in self.collection for val in vals.cosmetics_types)
    
    def get_unique_cosmetics_types(self):
        return tuple({val for vals in self.collection for val in vals.cosmetics_types})
    
    def get_certificates_attributes_as_tuples(self):
        return tuple((val,) for vals in self.collection for val in vals.certificates_attributes)
    
    def get_unique_certificates_attributes(self):
        return tuple({val for vals in self.collection for val in vals.certificates_attributes})
    
    def get_certified_items(self):
        return tuple((val.brand_id,) for val in self.collection if val.is_cf)
    
    def get_not_certified_items(self):
        return tuple((val.brand_id,) for val in self.collection if not val.is_cf)
    
    def add_brands_ids(self, collection: list[tuple]):
        for item in self.collection:
            for i in collection:
                if i[1] == item.name:
                    item.brand_id = i[0]
                    break
    
    def get_brands_with_types(self):
        result = []
        for val in self.collection:
            brand_types = val.get_brand_id_types()
            if brand_types is not None:
                result += brand_types
        
        return result

    def get_brands_certificates_attributes(self):
        result = []
        for val in self.collection:
            brand_ca = val.get_brand_id_certificats()
            if brand_ca is not None:
                result += brand_ca
        
        return result