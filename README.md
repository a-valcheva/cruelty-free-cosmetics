# Cruelty Free Cosmetics
I've created this project to learn and to experiment with web scraping and database design and development using Scrapy framework and MySQL with Python. 
The project is at a very early stage.
Next step is to replace MySQL with **MongoDB**.


## Concept and main functionalities
* Web scraping from the websites of cruelty free cosmetics brands accreditors - Cruelty Free Kitty (CFK), PETA, Leaping Bunny (LB), Cruelty Free International (CFI), with a separate spider dedicated to every each of them.
* Store scraped data in MySQL database.
* Backup of the database before running the spiders.

## Done by now
In this stage Cruelty Free Cosmetics has:
* One spider for one of the websites of the accreditors. 
* The data is stored in the intended tables in the database.
* Backup for the database.
* First sets of unit tests for the custom classes.
* Reading database credentials from txt file (to be replaced by .env file)

#### Database
<div align="left">
<img src="database-diagram.png" alt="logo" width="auto" height="auto" />
</div>

The database structure could be imported from cosmetics_cf.sql.
The database structure and data could be imported from one of the backup files in 'db_backup_files' folder.

## Technology description
Cruelty Free Cosmetics web scraping application is developed on Python 3.11.2

* Scrapy framework - used for the entire web scraping project.
* MySQL database - used for the storage of the scraped data. The communication with the MySQL server is provided using MySQL Connector/Python - a self-contained Python driver for that purpose.
* MySQL Workbench - for design and development the database.
* Unit tests - available for the custom classes and functions (in progress)
* ScrapeOps account - used instead of custom user-agent and proxy rotations.

## Next steps
* To add update functionality.
* There are several more spiders to be added for the rest of the accreditors' websites. This means more custom classes and changes in their design. There will be abstraction and inheritance included. 
* The database is also to be modified with additions. 
* User-agent and proxy rotations are to be added.
* To replace mysql_credentials.txt with .env file

And a lot more:)

## Settings
The project requires a txt file 'mysql_credentials.txt' to be created and filled with the database connection credentials and stored in the folder 'common'. The format needs to be as follows:
> [ host ]

> [ user ]

> [ password ]

> [ database ]
    
Every detail needs to be on a new line.

You also need to asign your ScrapeOps API Key to the constant SCRAPEOPS_API_KEY in the settings.py
You can easely get one with signing up at https://scrapeops.io
